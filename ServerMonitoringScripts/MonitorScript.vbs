strComputer = "."
ON ERROR RESUME NEXT
Const ForAppending = 8
Set objFSO = CreateObject("Scripting.FileSystemObject")
Set objWMIService = GetObject("winmgmts:" _
    & "{impersonationLevel=impersonate}!\\" & strComputer & "\root\cimv2")
Set objTextFile = objFSO.OpenTextFile _
    ("c:\ToBeEmailed.txt", ForAppending, True)

Set dtmConvertedDate = CreateObject("WbemScripting.SWbemDateTime")
Set colOperatingSystems = objWMIService.ExecQuery _
    ("Select * from Win32_OperatingSystem")
For Each objOperatingSystem in colOperatingSystems
    objTextFile.WriteLine(objOperatingSystem.Caption)
    objTextFile.WriteLine("")
    objTextFile.WriteLine("Build: " & objOperatingSystem.BuildNumber)
    objTextFile.WriteLine("Build Type: " & objOperatingSystem.BuildType)
    dtmConvertedDate.Value = objOperatingSystem.InstallDate
    dtmInstallDate = dtmConvertedDate.GetVarDate
    objTextFile.WriteLine("Install Date: " & dtmInstallDate)
    objTextFile.WriteLine("Licensed Users: " & _
        objOperatingSystem.NumberOfLicensedUsers)
    objTextFile.WriteLine( "Organization: " & objOperatingSystem.Organization)
    objTextFile.WriteLine("Primary: " & objOperatingSystem.Primary)
    objTextFile.WriteLine("Registered User: " & objOperatingSystem.RegisteredUser)
    objTextFile.WriteLine("Serial Number: " & objOperatingSystem.SerialNumber)
    objTextFile.WriteLine("Version: " & objOperatingSystem.Version)
    objTextFile.WriteLine("")
    objTextFile.WriteLine("")
Next

Set objSysInfo = CreateObject("ADSystemInfo")
objTextFile.WriteLine("Domain DNS name: " & objSysInfo.DomainDNSName)
objTextFile.WriteLine("")
objTextFile.WriteLine("Forest DNS name: " & objSysInfo.ForestDNSName)
objTextFile.WriteLine("")
objTextFile.WriteLine("PDC role owner: " & objSysInfo.PDCRoleOwner)
objTextFile.WriteLine("")
objTextFile.WriteLine("Schema role owner: " & objSysInfo.SchemaRoleOwner)
objTextFile.WriteLine("")
objTextFile.WriteLine("Domain is in native mode: " & objSysInfo.IsNativeMode)
objTextFile.WriteLine("")
objTextFile.WriteLine("User name: " & objSysInfo.UserName)
objTextFile.WriteLine("")
objTextFile.WriteLine("Computer name: " & objSysInfo.ComputerName)
objTextFile.WriteLine("")
objTextFile.WriteLine("Site name: " & objSysInfo.SiteName)
objTextFile.WriteLine("")
objTextFile.WriteLine("Domain short name: " & objSysInfo.DomainShortName)
objTextFile.WriteLine("")
objTextFile.WriteLine("")

Set colDrives = objFSO.Drives
For Each objDrive in colDrives
    objTextFile.WriteLine("Drive letter: " & objDrive.DriveLetter)
    objTextFile.WriteLine("Drive type: " & objDrive.DriveType)
    objTextFile.WriteLine("Total size: " & objDrive.TotalSize)
    objTextFile.WriteLine("Available space: " & objDrive.AvailableSpace)
    objTextFile.WriteLine("File system: " & objDrive.FileSystem)
    objTextFile.WriteLine("Free space: " & objDrive.FreeSpace)
    objTextFile.WriteLine("Is ready: " & objDrive.IsReady)
    objTextFile.WriteLine("Path: " & objDrive.Path)
    objTextFile.WriteLine("Root folder: " & objDrive.RootFolder)
    objTextFile.WriteLine("Serial number: " & objDrive.SerialNumber)
    objTextFile.WriteLine("Volume name: " & objDrive.VolumeName)
    objTextFile.WriteLine("")
Next
objTextFile.WriteLine("")
objTextFile.WriteLine("")
Set colProcessList = objWMIService.ExecQuery _
    ("Select * from Win32_Process Where Name = 'GRCDSPY.exe'")

For Each objProcess in colProcessList
    objProcess.Terminate()
Next

const ForReading = 1
dim strDEFSpy
dim objFS
dim objTS
set objFS = CreateObject("Scripting.FileSystemObject")
set objTS = objFS.OpenTextFile("c:\defspy.log", ForReading)
objTextFile.WriteLine(objTS.ReadAll)
objTextFile.WriteLine("")
objTextFile.WriteLine("")

set objFS = nothing
Set objTS = nothing

set objFS = CreateObject("Scripting.FileSystemObject")
set objTS = objFS.OpenTextFile("c:\cmdlinenetaudit.out", ForReading)
objTextFile.WriteLine(objTS.ReadAll)


set objFS = nothing
Set objTS = nothing

set objFS = CreateObject("Scripting.FileSystemObject")
set objTS = objFS.OpenTextFile("c:\Veritas.txt", ForReading)
objTextFile.WriteLine(objTS.ReadAll)


set objFS = nothing
Set objTS = nothing

set objFS = CreateObject("Scripting.FileSystemObject")
set objTS = objFS.OpenTextFile("c:\tobeemailed.txt", ForReading)
strReport = objTS.ReadAll
objTextFile.Close

set objFS = nothing
Set objTS = nothing

Set objEmail = CreateObject("CDO.Message")
objEmail.From = "ServerMonitoring@bahrcpa.com"
objEmail.To = "support@rushnow.com"
objEmail.Subject = "Server Monitoring" 
objEmail.Textbody = strReport
objEmail.Send