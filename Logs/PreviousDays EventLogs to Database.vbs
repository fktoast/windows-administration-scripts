strComputer = "."

Set objConn = CreateObject("ADODB.Connection")
Set objRS = CreateObject("ADODB.Recordset")
objConn.Open "DSN=EventLogs;"
objRS.CursorLocation = 3
objRS.Open "SELECT * FROM EventLogs" , objConn, 3, 3

Set dtmStartDate = CreateObject("WbemScripting.SWbemDateTime")
Set dtmEndDate = CreateObject("WbemScripting.SWbemDateTime")
DateToCheck = Date - 1
dtmEndDate.SetVarDate Date, True
dtmStartDate.SetVarDate DateToCheck, True

Set objWMIService = GetObject("winmgmts:" _
    & "{impersonationLevel=impersonate}!\\" & strComputer & "\root\cimv2")
Set colEvents = objWMIService.ExecQuery _
    ("Select * from Win32_NTLogEvent Where TimeWritten >= '" _ 
        & dtmStartDate & "' and TimeWritten < '" & dtmEndDate & "' and Logfile = 'Application'")
ON ERROR RESUME NEXT
For each objEvent in colEvents
    objRS.AddNew
    objRS("LogFileName") = "Application Log"
    objRS("Category") = objEvent.Category
    objRS("ComputerName") = objEvent.ComputerName
    objRS("EventCode") = objEvent.EventCode
    objRS("Message") = objEvent.Message
    objRS("RecordNumber") = objEvent.RecordNumber
    objRS("SourceName") = objEvent.SourceName
    objRS("TimeWritten") = objEvent.TimeWritten
    objRS("Type") = objEvent.Type
    objRS("User") = objEvent.User
    objRS.Update
Next
Set colEvents2 = objWMIService.ExecQuery _
    ("Select * from Win32_NTLogEvent Where TimeWritten >= '" _ 
        & dtmStartDate & "' and TimeWritten < '" & dtmEndDate & "' and Logfile = 'System'") 
For each objEvent2 in colEvents2
    objRS.AddNew
    objRS("LogFileName") = "System Log"
    objRS("Category") = objEvent2.Category
    objRS("ComputerName") = objEvent2.ComputerName
    objRS("EventCode") = objEvent2.EventCode
    objRS("Message") = objEvent2.Message
    objRS("RecordNumber") = objEvent2.RecordNumber
    objRS("SourceName") = objEvent2.SourceName
    objRS("TimeWritten") = objEvent2.TimeWritten
    objRS("Type") = objEvent2.Type
    objRS("User") = objEvent2.User
    objRS.Update
Next
Set colEvents3 = objWMIService.ExecQuery _
    ("Select * from Win32_NTLogEvent Where TimeWritten >= '" _ 
        & dtmStartDate & "' and TimeWritten < '" & dtmEndDate & "' and Logfile = 'Security'") 
For each objEvent3 in colEvents3
    objRS.AddNew
    objRS("LogFileName") = "Security Log"
    objRS("Category") = objEvent3.Category
    objRS("ComputerName") = objEvent3.ComputerName
    objRS("EventCode") = objEvent3.EventCode
    objRS("Message") = objEvent3.Message
    objRS("RecordNumber") = objEvent3.RecordNumber
    objRS("SourceName") = objEvent3.SourceName
    objRS("TimeWritten") = objEvent3.TimeWritten
    objRS("Type") = objEvent3.Type
    objRS("User") = objEvent3.User
    objRS.Update
Next
objRS.Close
objConn.Close

Set objConn = nothing
Set objRS = nothing
Set dtmStartDate = nothing
Set dtmEndDate = nothing
Set objWMIService = nothing
Set colEvents = nothing
Set colEvents2 = nothing
Set colEvents3 = nothing