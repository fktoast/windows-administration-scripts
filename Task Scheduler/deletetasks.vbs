Const ForReading = 1
Const OverwriteExisting = True
Set objArgs = WScript.Arguments
Set objNetwork = WScript.CreateObject("WScript.Network")
Set objShell = WScript.CreateObject("WScript.Shell")
Set objDictionary = CreateObject("Scripting.Dictionary")
Set objFSO = CreateObject("Scripting.FileSystemObject")
Set objTextFile = objFSO.OpenTextFile(objArgs(0), ForReading)
i = 0
 
Do While objTextFile.AtEndOfStream <> True
  strNextLine = objTextFile.Readline
  objDictionary.Add i, strNextLine
  i = i + 1
Loop
 
For Each objItem in objDictionary
  ON ERROR RESUME NEXT
  strcomputer = objDictionary.Item(objItem)
  Set objWMIService = GetObject("winmgmts:" _
      & "{impersonationLevel=impersonate}!\\" & strComputer & "\root\cimv2")
Set objWMIService = GetObject("winmgmts:" _
    & "{impersonationLevel=impersonate}!\\" & strComputer & "\root\cimv2")
Set colScheduledTasks = objWMIService.ExecQuery _
    ("Select * from Win32_ScheduledJob")
For Each objTask in colScheduledTasks
    intJobID = objTask.JobID
    Set objInstance = objWMIService.Get _
        ("Win32_ScheduledJob.JobID=" & intJobID)
    objInstance.Delete
Next
Next
Set objArgs = nothing
Set objNetwork = nothing
Set objShell = nothing
Set objDictionary = nothing
Set objFSO = nothing
Set objTextFile = nothing
