Const ForReading = 1
Const ForAppending = 8
Set objArgs = WScript.Arguments
Set objFSO = CreateObject("Scripting.FileSystemObject")
Set objDictionary = CreateObject("Scripting.Dictionary")
Set objTextFile = objFSO.OpenTextFile(objArgs(0), ForReading)
i = 0
Do While objTextFile.AtEndOfStream <> True
  strNextLine = objTextFile.Readline
  objDictionary.Add i, strNextLine
  i = i + 1
Loop
For Each objItem in objDictionary
  On Error Resume Next
  strcomputer = objDictionary.Item(objItem)
  Set objWMIService = GetObject _
      ("winmgmts:\\" & strComputer & "\root\cimv2:Win32_Process")
  Error = objWMIService.Create("cmd.exe /C cscript \\sharename\printer.vbs", null, null, intProcessID)
  If Error = 0 Then
      Wscript.Echo "cmd.exe was started with a process ID of " _
           & intProcessID & "."
  Else
      Wscript.Echo "cmd.exe could not be started due to error " & _
          Error & "."
  End If
Next
Set objWMIService = nothing
Set objStartup = nothing
Set objConfig = nothing