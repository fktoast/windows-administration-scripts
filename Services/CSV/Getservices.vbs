Const ForAppending = 8

Set objArgs = WScript.Arguments
Const ForReading = 1
Set objDictionary = CreateObject("Scripting.Dictionary")
Set objFSO = CreateObject("Scripting.FileSystemObject")
Set objTextFile = objFSO.OpenTextFile(objArgs(0), ForReading)
i = 0
Do While objTextFile.AtEndOfStream <> True
  strNextLine = objTextFile.Readline
  objDictionary.Add i, strNextLine
  i = i + 1
Loop

ON ERROR RESUME NEXT
For Each objItem in objDictionary
  Set colServices = GetObject("winmgmts://" & objDictionary.Item(objItem) _
      & "").ExecQuery("Select * from Win32_Service")
  Wscript.Echo colServices.Count
  strcomputer = objDictionary.Item(objItem)
  Set objLogFile = objFSO.OpenTextFile(strComputer & ".csv", _ 
      ForAppending, True)
  objLogFile.Write _
      ("System Name,Service Name,Service Type,Service State, Exit " _ 
          & "Code,Process ID,Can Be Paused,Can Be Stopped,Caption," _ 
          & "Description,Can Interact with Desktop,Display Name,Error " _
          & "Control, Executable Path Name,Service Started," _ 
          & "Start Mode,Account Name ") 
  objLogFile.Writeline
Set objWMIService = GetObject("winmgmts:" _
    & "{impersonationLevel=impersonate}!\\" & strComputer & "\root\cimv2")
Set colListOfServices = objWMIService.ExecQuery _
        ("Select * from Win32_Service")
For Each objService in colListOfServices
    objLogFile.Write(objService.SystemName) & "," 
    objLogFile.Write(objService.Name) & "," 
    objLogFile.Write(objService.ServiceType) & "," 
    objLogFile.Write(objService.State) & "," 
    objLogFile.Write(objService.ExitCode) & "," 
    objLogFile.Write(objService.ProcessID) & "," 
    objLogFile.Write(objService.AcceptPause) & "," 
    objLogFile.Write(objService.AcceptStop) & "," 
    objLogFile.Write(objService.Caption) & "," 
    objLogFile.Write(objService.Description) & "," 
    objLogFile.Write(objService.DesktopInteract) & "," 
    objLogFile.Write(objService.DisplayName) & "," 
    objLogFile.Write(objService.ErrorControl) & "," 
    objLogFile.Write(objService.PathName) & "," 
    objLogFile.Write(objService.Started) & "," 
    objLogFile.Write(objService.StartMode) & "," 
    objLogFile.Write(objService.StartName) & "," 
    objLogFile.writeline
Next
objLogFile.Close
Next
Set objArgs = nothing
Set objDictionary = nothing
Set objFSO = nothing
Set objTextFile = nothing
Set objLogFile = nothing
Set objWMIService = nothing