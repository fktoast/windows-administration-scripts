Const ForAppending = 8
Set objArgs = WScript.Arguments
Set objFSO = CreateObject("Scripting.FileSystemObject")
Const ForReading = 1
Set objDictionary = CreateObject("Scripting.Dictionary")
Set objTextFile = objFSO.OpenTextFile(objArgs(0), ForReading)
i = 0
 
Do While objTextFile.AtEndOfStream <> True
  strNextLine = objTextFile.Readline
  objDictionary.Add i, strNextLine
  i = i + 1
Loop
On Error Resume Next
For Each objItem in objDictionary.Item(objItem)
strcomputer = objDictionary.Item(objItem)
Set objWMIService = GetObject("winmgmts:" _
    & "{impersonationLevel=impersonate}!\\" & strComputer & "\root\cimv2")
Set colSubfolders = objWMIService.ExecQuery _
    ("Associators of {Win32_Directory.Name='c:\Program Files'} " _
        & "Where AssocClass = Win32_Subdirectory " _
            & "ResultRole = PartComponent")
Set objLogFile = objFSO.OpenTextFile("F:\subfoldersize.txt" , _ 
      ForAppending, True)
  objLogFile.Write _
      ("Computer,Subfolder Name,Subfolder Size") 
  objLogFile.Writeline
Set objFolder = objFSO.GetFolder("C:\Program Files")
For Each objSubfolder in colSubfolders
     objLogFile.Write(strcomputer) & ","
     objLogFile.Write(objSubfolder.Name) & ","
     objLogFile.Write(objSubfolder.Size) & ","
Next
Next
objLogFile.Close

Set objArgs = nothing
Set objFSO = nothing
Set objDictionary = nothing
Set objTextFile = nothing
Set objLogFile = nothing
Set objWMIService = nothing