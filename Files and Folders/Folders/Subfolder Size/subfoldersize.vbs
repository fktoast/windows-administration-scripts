Const ForAppending = 8
Set objFSO = CreateObject("Scripting.FileSystemObject")
Set objFolder = objFSO.GetFolder("C:\")
Set objLogFile = objFSO.OpenTextFile("SubFolders.csv", _ 
      ForAppending, True)
  objLogFile.Write _
      ("Subfolder Name,Subfolder Size") 
  objLogFile.Writeline
Set colSubfolders = objFolder.Subfolders
For Each objSubfolder in colSubfolders
     objLogFile.Write(objSubfolder.Name) & ","
     objLogFile.Write(objSubfolder.Size) & ","
     objLogFile.Writeline
Next
objLogFile.Close