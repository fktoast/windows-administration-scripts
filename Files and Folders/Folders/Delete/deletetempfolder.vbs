strComputer = "."
strTempPath = "C:\Documents and Settings\Frank Koch\Local Settings\Temp\"
strTempPath2 = "'\\Documents and Settings\\Frank Koch\\Local Settings\\Temp\\'"

Set objFSO = CreateObject("Scripting.FileSystemObject")
Set objFolder = objFSO.GetFolder(strTempPath)
Set colSubfolders = objFolder.Subfolders
Set objWMIService = GetObject("winmgmts:" _
    & "{impersonationLevel=impersonate}!\\" & strComputer & "\root\cimv2")
Set colFiles = objWMIService. _
    ExecQuery("Select * from CIM_DataFile where Path = " & strTempPath2)

ON ERROR RESUME NEXT

For Each objFile in colFiles
    strFilename = objFile.Name
    objFSO.DeleteFile(strFileName)
Next

For Each objSubfolder in colSubfolders
    strSubfolder = objSubfolder.Name
    objFSO.DeleteFolder(strTempPath & strSubfolder)
Next

Set objFSO = nothing
Set objFolder = nothing
Set colSubfolders = nothing
Set objWMIService = nothing
Set colFiles = nothing