Const OverwriteExisting = True
Const EVENT_SUCCESS = 0

Set objShell = Wscript.CreateObject("Wscript.Shell")
Set objFSO = CreateObject("Scripting.FileSystemObject")

strdate = datepart("yyyy", now) & "-" & datepart("m", now) _
& "-" & datepart("d", now)

If objFSO.FolderExists(strdate) Then
    objFSO.CopyFile "*.mdb" , strdate , OverwriteExisting
    objShell.LogEvent EVENT_SUCCESS, _
    "Database copy script ran successfully."
Else
    Set objFolder = objFSO.CreateFolder(strdate)
    objFSO.CopyFile "*.mdb" , strdate , OverwriteExisting
    objShell.LogEvent EVENT_SUCCESS, _
    "Created folder " & strdate & ".  " & _
    "Database copy script ran successfully."
End If

Set objShell = nothing
Set objFSO = nothing