Set objExplorer = WScript.CreateObject("InternetExplorer.Application")
objExplorer.Navigate "about:blank"   
objExplorer.ToolBar = 0
objExplorer.StatusBar = 0
objExplorer.Width=450
objExplorer.Height = 150 
objExplorer.Left = 0
objExplorer.Top = 0

Do While (objExplorer.Busy)
    Wscript.Sleep 600
Loop    

objExplorer.Visible = 1             
objExplorer.Document.Body.InnerHTML = "Retrieving service information. " _
    & "This might take several minutes to complete."

Const CONVERSION_FACTOR = 1048576
Const WARNING_THRESHOLD = 100
Const ForReading = 1
Set objArgs = WScript.Arguments
Set objDictionary = CreateObject("Scripting.Dictionary")
Set objFSO = CreateObject("Scripting.FileSystemObject")
Set objTextFile = objFSO.OpenTextFile(objArgs(0), ForReading)
i = 0
  
Do While objTextFile.AtEndOfStream <> True
  strNextLine = objTextFile.Readline
  objDictionary.Add i, strNextLine
  i = i + 1
Loop
For Each objItem in objDictionary
ON ERROR RESUME NEXT
Set objWMIService = GetObject("winmgmts:\\" & objDictionary.Item(objItem) & "\root\cimv2")
strComputer = objDictionary.Item(objItem)
Set colNetCards = objWMIService.ExecQuery _
    ("Select * From Win32_NetworkAdapterConfiguration Where IPEnabled = True")
  For Each objNetCard in colNetCards
    arrDNSServers = Array("192.168.2.102")
    strSetting = objNetCard.SetDNSServerSearchOrder(arrDNSServers)
    If strSetting = 0 _
    Then 
      strevent = "Success"
    Else
      strevent = "Failed"
    End if
    strOutput = strOutput & strcomputer & VbTab & " ------- " & VbTab & strevent & "<br>"
  Next
Next
objExplorer.Height = 400
objExplorer.Document.Body.InnerHTML = stroutput
Set objectexplorer = nothing
Set objDictionary = nothing
Set objFSO = nothing
Set objTextFile = nothing
Set objWMIService = nothing
Set objargs= nothing
