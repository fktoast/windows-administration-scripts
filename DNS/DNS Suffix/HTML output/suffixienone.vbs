Set objExplorer = WScript.CreateObject("InternetExplorer.Application")
objExplorer.Navigate "about:blank"   
objExplorer.ToolBar = 0
objExplorer.StatusBar = 0
objExplorer.Width=400
objExplorer.Height = 400 
objExplorer.Left = 0
objExplorer.Top = 0

Do While (objExplorer.Busy)
    Wscript.Sleep 600
Loop    

objExplorer.Visible = 1             
objExplorer.Document.Body.InnerHTML = "Retrieving service information. " _
    & "This might take several minutes to complete."

Const CONVERSION_FACTOR = 1048576
Const WARNING_THRESHOLD = 100

Set objArgs = WScript.Arguments
Const ForReading = 1
Set objDictionary = CreateObject("Scripting.Dictionary")
Set objFSO = CreateObject("Scripting.FileSystemObject")
Set objTextFile = objFSO.OpenTextFile(objArgs(0), ForReading)
i = 0
 
Do While objTextFile.AtEndOfStream <> True
  strNextLine = objTextFile.Readline
  objDictionary.Add i, strNextLine
  i = i + 1
Loop
On Error Resume Next 
For Each objItem in objDictionary
	Set objWMIService = GetObject("winmgmts:\\" & objDictionary.Item(objItem) _
	    & "")
	strname = "Computer: " & objDictionary.Item(objItem)
        Set colNetCards = objWMIService.ExecQuery _
		("Select * From Win32_NetworkAdapterConfiguration Where IPEnabled = True")
	For Each objNetCard in colNetCards
            strevent = objNetCard.SetDNSDomain("")
            If strevent = 0 _
            Then            
                strStatus = "Success"
            Else
                strStatus = "Failed"
            End if
            stroutput = stroutput & strname & VbTab & " ------- " & VbTab & strStatus & "<br>"
        Next
Next

objExplorer.Document.Body.InnerHTML = stroutput

Set objectexplorer = nothing
Set objDictionary = nothing
Set objFSO = nothing
Set objWMIService = nothing
Set objTextFile = nothing
Wscript.Quit