Const CONVERSION_FACTOR = 1048576
Const WARNING_THRESHOLD = 100

If WScript.Arguments.Count = 0 Then
    Wscript.Echo "Usage: FirstScript.vbs server1 [server2] [server3] ..."
    WScript.Quit
End If

For Each strComputer In WScript.Arguments
	On Error Resume Next
	Set objWMIService = GetObject("winmgmts:\\" & strComputer & "\root\cimv2")
	Set colNetCards = objWMIService.ExecQuery _
		("Select * From Win32_NetworkAdapterConfiguration Where IPEnabled = True")
	For Each objNetCard in colNetCards
		objNetCard.SetDNSDomain("")
	Next
Next
