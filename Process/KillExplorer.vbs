strComputer = "."
Set objWMIService = GetObject("winmgmts:" _
    & "{impersonationLevel=impersonate}!\\" & strComputer & "\root\cimv2")
Set colProcessList = objWMIService.ExecQuery _
    ("Select * from Win32_Process Where Name = 'explorer.exe'")
For Each objProcess in colProcessList
    objProcess.Terminate()
Next
Set colMonitoredProcesses = objWMIService. _        
    ExecNotificationQuery("select * from __instancecreationevent " _ 
        & " within 1 where TargetInstance isa 'Win32_Process'")
i = 0
Do While i = 0
    Set objLatestProcess = colMonitoredProcesses.NextEvent
    If objLatestProcess.TargetInstance.Name = "explorer.exe" Then
    objLatestProcess.TargetInstance.Terminate
    End If
Loop