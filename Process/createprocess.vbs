strComputer = "as5"
Set objWMIService = GetObject _
    ("winmgmts:\\" & strComputer & "\root\cimv2:Win32_Process")

Error = objWMIService.Create("cmd.exe /C printer.vbs", null, null, intProcessID)
If Error = 0 Then
    Wscript.Echo "Exchange was started with a process ID of " _
         & intProcessID & "."
Else
    Wscript.Echo "Exchange could not be started due to error " & _
        Error & "."
End If

Set objWMIService = nothing
Set objStartup = nothing
Set objConfig = nothing