::@echo off
::***********************************
:: Script: copyacross_nodeinfo.cmd
:: Purpose: Command for moving or replacing files
::***********************************

echo Now running the CopyAcross for ITO Nodeinfo...
:: Moving/Replacing files
FOR /F %%t IN (c:\servermoves\move6\filelist.txt) DO (
      if not exist c:\servermoves\move6\%%t md c:\servermoves\move6\%%t
>>c:\servermoves\move6\foldercreate.log
      if not exist \\%%t\c$\usr\OV\conf\OpC echo ITO Folder is missing >>c: \servermoves\move6\%%t_no_ito.log
      if exist \\%%t\c$\usr\OV\conf\Opc\ net use x: \\%%t\c $\usr\OV\conf\OpC >>c:\servermoves\move6\%%t\%%t_nodeinfo_share.log
      if exist x: echo %%t has X drive mapped >>c: \servermoves\move6\%%t\drive.log
      if not exist x: echo %%t doesn't have X drive mapped >>c: \servermoves\move6\%%t\nodrive.log
      if exist x: psservice \\%%t stop "hp ito agent" >>c: \servermoves\move6\%%t\%%t_ito_stop.log
      sleep 5
      echo %%t
      ren c:\servermoves\nodeinfo\move6\%%t.txt %%t
      xcopy c:\servermoves\nodeinfo\move6\%%t c: \servermoves\move6\%%t\nodeinfo /F
      if exist x:\nodeinfo echo %%t Currently has file >>c: \servermoves\move6\%%t\filelocated.log
      xcopy c:\servermoves\move6\%%t\nodeinfo x: >>c: \servermoves\move6\%%t\%%t_updated_nodeinfo.log
      if not exist x:\nodeinfo xcopy c:\servermoves\move6\%%t\nodeinfo x:
>>c:\servermoves\move6\%%t\%%t_new_nodeinfo.log
      psservice \\%%t restart "hp ito agent" >>c: \servermoves\move6\%%t\%%t_ito_restart.log
      if exist x: net use x: /delete >>c: \servermoves\move6\%%t\%%t_nodeinfo_share.log
      )


