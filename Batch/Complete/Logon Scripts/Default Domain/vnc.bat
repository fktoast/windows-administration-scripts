@echo off

if exist c:\progra~1\realvnc\winvnc.exe goto End
if not exist c:\progra~1\realvnc\winvnc.exe goto install

:install
if exist j: net use j: /d 
if not exist j: net use j: \\dc2\data 
if not exist c:\progra~1\realvnc\ md c:\progra~1\realvnc\
if not exist c:\progra~1\realvnc\winvnc.exe copy j:\software\vnc\EXE\*.* c:\progra~1\realvnc\
regedit /s c:\progra~1\realvnc\winvnc.reg
erase c:\progra~1\realvnc\*.reg /Q
c:\progra~1\realvnc\winvnc.exe -install
net start winvnc
net use j: /d
goto End

:End
Echo ----------------------------
Echo WinVNC is installed!
Echo ____________________________
Echo .
Sleep 2
@exit