Const ForAppending = 8
Set objArgs = WScript.Arguments
Const ForReading = 1
Set objDictionary = CreateObject("Scripting.Dictionary")
Set objFSO = CreateObject("Scripting.FileSystemObject")
Set objTextFile = objFSO.OpenTextFile(objArgs(0), ForReading)
i = 0
 
Do While objTextFile.AtEndOfStream <> True
  strNextLine = objTextFile.Readline
  objDictionary.Add i, strNextLine
  i = i + 1
Loop

For Each objItem in objDictionary
ON ERROR RESUME NEXT
strcomputer = objDictionary.Item(objItem)
Set objWMIService = GetObject("winmgmts:\\" & strComputer _
    & "\root\cimv2\Applications\MicrosoftIE")
Set colIESettings = objWMIService.ExecQuery _
    ("Select * from MicrosoftIE_LANSettings")
Set objLogFile = objFSO.OpenTextFile("C:\FKOCH\Administrative Archive\Microsoft\" _
      & "Scripts\VBScript\WMI\Complete\IE\Logs\IELANSettings_TLC.csv", _ 
      ForAppending, True)
  objLogFile.Write _
      ("Computer,Autoconfiguration proxy,Autoconfiguration URL,Autoconfiguration " _
      & "Proxy detection mode,Proxy,Proxy Override,Proxy Server") 
  objLogFile.Writeline
For Each strIESetting in colIESettings
    objLogFile.Write(strcomputer) & ","
    objLogFile.Write(strIESetting.AutoConfigProxy) & ","
    objLogFile.Write(strIESetting.AutoConfigURL) & ","
    objLogFile.Write(strIESetting.AutoProxyDetectMode) & ","
    objLogFile.Write(strIESetting.Proxy) & ","
    objLogFile.Write(strIESetting.ProxyOverride) & ","
    objLogFile.Write(strIESetting.ProxyServer) & ","
    objLogFile.Writeline
Next
objLogFile.Close
Set objWMIService = GetObject("winmgmts:\\" & strComputer _
    & "\root\cimv2\Applications\MicrosoftIE")
Set colIESecs = objWMIService.ExecQuery _
    ("Select * from MicrosoftIE_Security")
Set objLogFile = objFSO.OpenTextFile("C:\FKOCH\Administrative Archive\Microsoft\" _
      & "Scripts\VBScript\WMI\Complete\IE\Logs\IESecurity_TLC.csv", _ 
      ForAppending, True)
  objLogFile.Write _
      ("Computer,Zone Name,Security Level") 
  objLogFile.Writeline
For Each strIESec in colIESecs
    objLogFile.Write(strcomputer) & ","
    objLogFile.Write(strIEsec.Zone) & ","
    objLogFile.Write(strIEsec.Level) & ","
    objLogFile.Writeline
Next
objLogFile.Close
Set objWMIService = GetObject("winmgmts:\\" & strComputer _
    & "\root\cimv2\Applications\MicrosoftIE")
Set colIESums = objWMIService.ExecQuery _
    ("Select * from MicrosoftIE_Summary")
Set objLogFile = objFSO.OpenTextFile("C:\FKOCH\Administrative Archive\Microsoft\" _
      & "Scripts\VBScript\WMI\Complete\IE\Logs\IESummary_TLC.csv", _ 
      ForAppending, True)
  objLogFile.Write _
      ("Computer,Active printer,Build,Cipher strength,Content advisor," _
      & "IE Administration Kit installed,Language,Name,Path,Product ID,Version") 
  objLogFile.Writeline
For Each strIESum in colIESums
    objLogFile.Write(strcomputer) & ","
    objLogFile.Write(strIESum.ActivePrinter) & ","
    objLogFile.Write(strIESum.Build) & ","
    objLogFile.Write(strIESum.CipherStrength) & ","
    objLogFile.Write(strIESum.ContentAdvisor) & ","
    objLogFile.Write(strIESum.IEAKInstall) & ","
    objLogFile.Write(strIESum.Language) & ","
    objLogFile.Write(strIESum.Name) & ","
    objLogFile.Write(strIESum.Path) & ","
    objLogFile.Write(strIESum.ProductID) & ","
    objLogFile.Write(strIESum.Version) & ","
    objLogFile.Writeline
Next
objLogFile.Close
Set objWMIService = GetObject("winmgmts:\\" & strComputer _
    & "\root\cimv2\Applications\MicrosoftIE")
Set colIEObjects = objWMIService.ExecQuery _
    ("Select * from MicrosoftIE_Object")
Set objLogFile = objFSO.OpenTextFile("C:\FKOCH\Administrative Archive\Microsoft\" _
      & "Scripts\VBScript\WMI\Complete\IE\Logs\IEObject_TLC.csv", _ 
      ForAppending, True)
  objLogFile.Write _
      ("Computer,Code Base,Program File,Status") 
  objLogFile.Writeline
For Each strIEObject in colIEObjects
    objLogFile.Write(strcomputer) & ","
    objLogFile.Write(strIEObject.CodeBase) & ","
    objLogFile.Write(strIEObject.ProgramFile) & ","
    objLogFile.Write(strIEObject.Status) & ","
    objLogFile.Writeline
Next
objLogFile.Close
Set objWMIService = GetObject("winmgmts:\\" & strComputer _
    & "\root\cimv2\Applications\MicrosoftIE")
Set colIECSS = objWMIService.ExecQuery _
    ("Select * from MicrosoftIE_ConnectionSettings")
Set objLogFile = objFSO.OpenTextFile("C:\FKOCH\Administrative Archive\Microsoft\" _
      & "Scripts\VBScript\WMI\Complete\IE\Logs\IEConnectionSettings_TLC.csv", _ 
      ForAppending, True)
  objLogFile.Write _
      ("Computer,Allow Internet programs,Autoconfiguration URL,Auto disconnect,Autoconfiguration proxy detection mode," _
      & "Data encryption,Default,Default Gateway,Dialup server,Disconnect idle time," _
      & "Encrypted password,IP address,IP header compression,Modem,Name,Network logon," _
      & "Network protocols,Primary DNS server,Primary WINS server,Proxy,Proxy override," _
      & "Proxy server,Redial attempts,Redial wait,Script fileame,Secondary DNS server," _
      & "Secondary WINS server,Server assigned IP address,Server assigned name server," _
      & "Software compression")
  objLogFile.Writeline
For Each strIECS in colIECSS
    objLogFile.Write(strcomputer) & ","
    objLogFile.Write(striecs.AllowInternetPrograms) & ","
    objLogFile.Write(striecs.AutoConfigURL) & ","
    objLogFile.Write(striecs.AutoDisconnect) & ","
    objLogFile.Write(striecs.AutoProxyDetectMode) & ","
    objLogFile.Write(striecs.DataEncryption) & ","
    objLogFile.Write(striecs.Default) & ","
    objLogFile.Write(striecs.DefaultGateway) & ","
    objLogFile.Write(striecs.DialUpServer) & ","
    objLogFile.Write(striecs.DisconnectIdleTime) & ","
    objLogFile.Write(striecs.EncryptedPassword) & ","
    objLogFile.Write(striecs.IPAddress) & ","
    objLogFile.Write(striecs.IPHeaderCompression) & ","
    objLogFile.Write(striecs.Modem) & ","
    objLogFile.Write(striecs.Name) & ","
    objLogFile.Write(striecs.NetworkLogon) & ","
    objLogFile.Write(striecs.NetworkProtocols) & ","
    objLogFile.Write(striecs.PrimaryDNS) & ","
    objLogFile.Write(striecs.PrimaryWINS) & ","
    objLogFile.Write(striecs.Proxy) & ","
    objLogFile.Write(striecs.ProxyOverride) & ","
    objLogFile.Write(striecs.ProxyServer) & ","
    objLogFile.Write(striecs.RedialAttempts) & ","
    objLogFile.Write(striecs.RedialWait) & ","
    objLogFile.Write(striecs.ScriptFileName) & ","
    objLogFile.Write(striecs.SecondaryDNS) & ","
    objLogFile.Write(striecs.SecondaryWINS) & ","
    objLogFile.Write(striecs.ServerAssignedIPAddress) & ","
    objLogFile.Write(striecs.ServerAssignedNameServer) & ","
    objLogFile.Write(striecs.SoftwareCompression) & ","
    objLogFile.Writeline
Next
objLogFile.Close
Set objWMIService = GetObject("winmgmts:\\" & strComputer _
    & "\root\cimv2\Applications\MicrosoftIE")
Set colIEConSums = objWMIService.ExecQuery _
    ("Select * from MicrosoftIE_ConnectionSummary")
Set objLogFile = objFSO.OpenTextFile("C:\FKOCH\Administrative Archive\Microsoft\" _
      & "Scripts\VBScript\WMI\Complete\IE\Logs\IEConnectionSummary_TLC.csv", _ 
      ForAppending, True)
  objLogFile.Write _
      ("Computer,Connection Preferences,HTTP1.1 Enabled,Proxy HTTP 1.1 Enabled") 
  objLogFile.Writeline
For Each strIEConSum in colIEConSums
    objLogFile.Write(strcomputer) & ","
    objLogFile.Write(strIEConSum.ConnectionPreference) & ","
    objLogFile.Write(strIEConSum.EnableHTTP11) & ","
    objLogFile.Write(strIEConSum.ProxyHTTP11) & ","
    objLogFile.Writeline
Next
Next
objLogFile.Close

Set objArgs = nothing
Set objFSO = nothing
Set objDictionary = nothing
Set objTextFile = nothing
Set objLogFile = nothing