const HKEY_CURRENT_USER = &H80000001
const HKEY_USERS = &H80000003
const HKEY_LOCAL_MACHINE = &H80000002
Const ForAppending = 8
Set objArgs = WScript.Arguments
Set objFSO = CreateObject("Scripting.FileSystemObject")
Const ForReading = 1
Set objDictionary = CreateObject("Scripting.Dictionary")
Set objTextFile = objFSO.OpenTextFile(objArgs(0), ForReading)
i = 0
 
Do While objTextFile.AtEndOfStream <> True
  strNextLine = objTextFile.Readline
  objDictionary.Add i, strNextLine
  i = i + 1
Loop


For Each objItem in objDictionary
On Error Resume Next
strcomputer = objDictionary.Item(objItem)
Set oReg=GetObject("winmgmts:{impersonationLevel=impersonate}!\\" &_ 
strComputer & "\root\default:StdRegProv")
strKeyPath = "SOFTWARE\Microsoft\Internet Explorer\Main"
strValueName = "Start Page"
strValue = "http://www.corp.leatherfactory.com/"
oReg.SetStringValue HKEY_CURRENT_USER,strKeyPath,strValueName,strValue
oReg.SetStringValue HKEY_USERS,strKeyPath,strValueName,strValue
oReg.SetStringValue HKEY_LOCAL_MACHINE,strKeyPath,strValueName,strValue
Next
Set objArgs = nothing
Set objFSO = nothing
Set objDictionary = nothing
Set objTextFile = nothing
Set objLogFile = nothing