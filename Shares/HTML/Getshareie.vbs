Set objExplorer = WScript.CreateObject("InternetExplorer.Application")
objExplorer.Navigate "about:blank"   
objExplorer.ToolBar = 0
objExplorer.StatusBar = 0
objExplorer.Width=400
objExplorer.Height = 400 
objExplorer.Left = 0
objExplorer.Top = 0

Do While (objExplorer.Busy)
    Wscript.Sleep 600
Loop    

objExplorer.Visible = 1             
objExplorer.Document.Body.InnerHTML = "Retrieving service information. " _
    & "This might take several minutes to complete."
Set objArgs = WScript.Arguments
Const ForReading = 1
Set objDictionary = CreateObject("Scripting.Dictionary")
Set objFSO = CreateObject("Scripting.FileSystemObject")
Set objTextFile = objFSO.OpenTextFile(objArgs(0), ForReading)
i = 0
 
Do While objTextFile.AtEndOfStream <> True
  strNextLine = objTextFile.Readline
  objDictionary.Add i, strNextLine
  i = i + 1
Loop
 
For Each objItem in objDictionary
	Set objWMIService = GetObject("winmgmts:" _
	    & "{impersonationLevel=impersonate}!//" & objDictionary.Item(objItem) _
	    & "")
	Set colShares = objWMIService.ExecQuery("Select * from Win32_Share")
Next

For each objShare in colShares
    strTemp = strtemp & "Computer: " & vbTab & objDictionary.Item(objItem) & "<br>"
    strTemp = strtemp & "Caption: " & vbTab &  objShare.Caption & "<br>"  
    strTemp = strtemp & "Name: " & vbTab &  objShare.Name & "<br>"   
    strTemp = strtemp & "Path: " & vbTab &  objShare.Path & "<br>"  
    strTemp = strtemp & "Type: " & vbTab &  objShare.Type & "<br><br>"  	     
Next

objExplorer.Document.Body.InnerHTML = strtemp

Wscript.Sleep 3000
Set objectexplorer = nothing
Set objDictionary = nothing
Set objFSO = nothing
Set objWMIService = nothing
Set objTextFile = nothing

Wscript.Quit
