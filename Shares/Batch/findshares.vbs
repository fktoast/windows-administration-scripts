Const CONVERSION_FACTOR = 1048576
Const WARNING_THRESHOLD = 100

If WScript.Arguments.Count = 0 Then
    Wscript.Echo "Usage: FirstScript.vbs server1 [server2] [server3] ..."
    WScript.Quit
End If

For Each strComputer In WScript.Arguments
    Set objWMIService = GetObject("winmgmts:" _
    & "{impersonationLevel=impersonate}!\\" & strComputer & "\root\cimv2")
    Set colShares = objWMIService.ExecQuery("Select * from Win32_Share")
    For each objShare in colShares
    	Wscript.Echo "AllowMaximum: " & vbTab &  objShare.AllowMaximum   
    	Wscript.Echo "Caption: " & vbTab &  objShare.Caption   
    	Wscript.Echo "MaximumAllowed: " & vbTab &  objShare.MaximumAllowed
    	Wscript.Echo "Name: " & vbTab &  objShare.Name   
    	Wscript.Echo "Path: " & vbTab &  objShare.Path   
    	Wscript.Echo "Type: " & vbTab &  objShare.Type   
     Next
Next