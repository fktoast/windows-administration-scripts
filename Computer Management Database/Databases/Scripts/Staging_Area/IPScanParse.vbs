Const ForReading = 1

Const adOpenStatic = 3
Const adLockOptimistic = 3
Const adUseClient = 3
Set objConnection = CreateObject("ADODB.Connection")
Set objRecordset = CreateObject("ADODB.Recordset")
objConnection.Open "DSN=Computer_Management;"

Set objFSO = CreateObject("Scripting.FileSystemObject")
Set objTextFile = objFSO.OpenTextFile("z:\databases\computer_management\" _
    & "computers.csv", ForReading)
objRecordset.CursorLocation = adUseClient
  objRecordset.Open "SELECT * FROM IPScan " , objConnection, _
    adOpenStatic, adLockOptimistic
Do Until objTextFile.AtEndOfStream
    arrIPScan = split(objTextFile.Readline, ",")
    objRecordset.AddNew
    objRecordset("IPAddress") = arrIPScan(0)
    objRecordset("PingTime") = arrIPScan(1)
    objRecordset("HostName") = arrIPScan(2)
    objRecordset("ComputerName") = arrIPScan(3)
    objRecordset("DomainName") = arrIPScan(4)
    objRecordset("UserName") = arrIPScan(5)
    objRecordset("MacAddress") = arrIPScan(6)
    objRecordset("TTL") = arrIPScan(7)
Loop
objRecordset.Update

objRecordset.Close
objConnection.Close

Set objArgs = nothing
Set objFSO = nothing
Set objDictionary = nothing
Set objTextFile = nothing
Set objLogFile = nothing
Set objWMIService = nothing