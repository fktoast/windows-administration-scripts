Const ForReading = 1
Const ForAppending = 8
Const adOpenStatic = 3
Const adLockOptimistic = 3
Const adUseClient = 3

Set objConnection = CreateObject("ADODB.Connection")
Set objRecordset = CreateObject("ADODB.Recordset")

Set objArgs = WScript.Arguments
Set objFSO = CreateObject("Scripting.FileSystemObject")
Set objDictionary = CreateObject("Scripting.Dictionary")
Set objTextFile = objFSO.OpenTextFile(objArgs(0), ForReading)
i = 0
 
Do While objTextFile.AtEndOfStream <> True
  strNextLine = objTextFile.Readline
  objDictionary.Add i, strNextLine
  i = i + 1
Loop

objConnection.Open "DSN=Computer_Management;"

For Each objItem in objDictionary
  On Error Resume Next
  strcomputer = objDictionary.Item(objItem)
  Set objWMIService = GetObject("winmgmts:" _
    & "{impersonationLevel=impersonate}!\\" & strComputer & "\root\cimv2")
  set objRefresher = CreateObject("WbemScripting.SWbemRefresher")
  Set colDisks = objRefresher.AddEnum _
    (objWMIService, "win32_perfformatteddata_perfdisk_physicaldisk"). _
        objectSet
  objRefresher.Refresh
  objRecordset.CursorLocation = adUseClient
  objRecordset.Open "SELECT * win32_perfformatteddata_perfdisk_physicaldisk " , objConnection, _
    adOpenStatic, adLockOptimistic 
  For i = 1 to 100
    For Each objDisk in colDisks
        objRecordset.AddNew
        objRecordset("strcomputer") = strcomputer
        objRecordset("objDisk_AvgDiskBytesPerRead") = objDisk.AvgDiskBytesPerRead
        objRecordset("objDisk_AvgDiskBytesPerTransfer") = objDisk.AvgDiskBytesPerTransfer
        objRecordset("objDisk_AvgDiskBytesPerWrite") = objDisk.AvgDiskBytesPerWrite
        objRecordset("objDisk_AvgDiskQueueLength") = objDisk.AvgDiskQueueLength
        objRecordset("objDisk_AvgDiskReadQueueLength") = objDisk.AvgDiskReadQueueLength
        objRecordset("objDisk_AvgDiskSecPerRead") = objDisk.AvgDiskSecPerRead
        objRecordset("objDisk_AvgDiskSecPerTransfer") = objDisk.AvgDiskSecPerTransfer
        objRecordset("objDisk_AvgDiskSecPerWrite") = objDisk.AvgDiskSecPerWrite
        objRecordset("objDisk_AvgDiskWriteQueueLength") = objDisk.AvgDiskWriteQueueLength
        objRecordset("objDisk_CurrentDiskQueueLength") = objDisk.CurrentDiskQueueLength
        objRecordset("objDisk_DiskBytesPerSec") = objDisk.DiskBytesPerSec
        objRecordset("objDisk_DiskReadBytesPerSec") = objDisk.DiskReadBytesPerSec
        objRecordset("objDisk_DiskReadsPerSec") = objDisk.DiskReadsPerSec
        objRecordset("objDisk_DiskTransfersPerSec") = objDisk.DiskTransfersPerSec
        objRecordset("objDisk_DiskWriteBytesPerSec") = objDisk.DiskWriteBytesPerSec
        objRecordset("objDisk_DiskWritesPerSec") = objDisk.DiskWritesPerSec
        objRecordset("objDisk_FreeMegabytes") = objDisk.FreeMegabytes
        objRecordset("objDisk_Name") = objDisk.Name
        objRecordset("objDisk_PercentDiskReadTime") = objDisk.PercentDiskReadTime
        objRecordset("objDisk_PercentDiskTime") = objDisk.PercentDiskTime
        objRecordset("objDisk_PercentDiskWriteTime") = objDisk.PercentDiskWriteTime
        objRecordset("objDisk_PercentFreeSpace") = objDisk.PercentFreeSpace
        objRecordset("objDisk_PercentIdleTime") = objDisk.PercentIdleTime
        objRecordset("objDisk_SplitIOPerSec") = objDisk.SplitIOPerSec
        Wscript.Sleep 2000
        objRefresher.Refresh
        objRecordset.Update
    Next
  Next
Next

objRecordset.Close
objConnection.Close

Set objArgs = nothing
Set objFSO = nothing
Set objDictionary = nothing
Set objTextFile = nothing
Set objLogFile = nothing
Set objWMIService = nothing