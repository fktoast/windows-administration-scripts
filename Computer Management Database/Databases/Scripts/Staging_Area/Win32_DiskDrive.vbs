Const ForReading = 1
Const ForAppending = 8
Const adOpenStatic = 3
Const adLockOptimistic = 3
Const adUseClient = 3

Set objConnection = CreateObject("ADODB.Connection")
Set objRecordset = CreateObject("ADODB.Recordset")

Set objArgs = WScript.Arguments
Set objFSO = CreateObject("Scripting.FileSystemObject")
Set objDictionary = CreateObject("Scripting.Dictionary")
Set objTextFile = objFSO.OpenTextFile(objArgs(0), ForReading)
i = 0
 
Do While objTextFile.AtEndOfStream <> True
  strNextLine = objTextFile.Readline
  objDictionary.Add i, strNextLine
  i = i + 1
Loop

objConnection.Open "DSN=Computer_Management;"
For Each objItem in objDictionary
  On Error Resume Next
  strcomputer = objDictionary.Item(objItem)
  Set objWMIService = GetObject("winmgmts") =" _    
    & "{impersonationLevel=impersonate}!\\" & strComputer _
        & "\root\cimv2")
  Set colDiskDrives = objWMIService.ExecQuery _    
    ("Select * from Win32_DiskDrive")
  objRecordset.CursorLocation = adUseClient
  objRecordset.Open "SELECT * FROM Win32_DiskDrive " , objConnection, _
    adOpenStatic, adLockOptimistic 

  For each objDiskDrive in colDiskDrives
    objRecordset.AddNew
    objRecordset("strcomputer") = strcomputer
    objRecordset("objDiskDrive_BytesPerSector") = objDiskDrive.BytesPerSector        
    For i = Lbound(objDiskDrive.Capabilities) to _
        Ubound(objDiskDrive.Capabilities)
        objRecordset("objDiskDrive_Capabilities") = objDiskDrive.Capabilities(i)
    Next    
    objRecordset("objDiskDrive_Caption") = objDiskDrive.Caption
    objRecordset("objDiskDrive_DeviceID") = objDiskDrive.DeviceID
    objRecordset("objDiskDrive_Index") = objDiskDrive.Index
    objRecordset("objDiskDrive_InterfaceType") = objDiskDrive.InterfaceType
    objRecordset("objDiskDrive_Manufacturer") = objDiskDrive.Manufacturer
    objRecordset("objDiskDrive_MediaLoaded") = objDiskDrive.MediaLoaded
    objRecordset("objDiskDrive_MediaType") = objDiskDrive.MediaType
    objRecordset("objDiskDrive_Model") = objDiskDrive.Model
    objRecordset("objDiskDrive_Name") = objDiskDrive.Name
    objRecordset("objDiskDrive_Partitions") = objDiskDrive.Partitions
    objRecordset("objDiskDrive_PNPDeviceID") = objDiskDrive.PNPDeviceID
    objRecordset("objDiskDrive_SCSIBus") = objDiskDrive.SCSIBus
    objRecordset("objDiskDrive_SCSILogicalUnit") = objDiskDrive.SCSILogicalUnit
    objRecordset("objDiskDrive_SCSIPort") = objDiskDrive.SCSIPort
    objRecordset("objDiskDrive_SCSITargetId") = objDiskDrive.SCSITargetId    
    objRecordset("objDiskDrive_SectorsPerTrack") = objDiskDrive.SectorsPerTrack        
    objRecordset("objDiskDrive_Signature") = objDiskDrive.Signature          
    objRecordset("objDiskDrive_Size") = objDiskDrive.Size     
    objRecordset("objDiskDrive_Status") = objDiskDrive.Status         
    objRecordset("objDiskDrive_TotalCylinders") = objDiskDrive.TotalCylinders         
    objRecordset("objDiskDrive_TotalHeads") = objDiskDrive.TotalHeads    
    objRecordset("objDiskDrive_TotalSectors") = objDiskDrive.TotalSectors
    objRecordset("objDiskDrive_TotalTracks") = objDiskDrive.TotalTracks
    objRecordset("objDiskDrive_TracksPerCylinder") = objDiskDrive.TracksPerCylinder  
    objRecordset.Update
  Next
Next
objRecordset.Close
objConnection.Close

Set objArgs = nothing
Set objFSO = nothing
Set objDictionary = nothing
Set objTextFile = nothing
Set objLogFile = nothing
Set objWMIService = nothing