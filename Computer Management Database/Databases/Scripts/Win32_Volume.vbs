Const ForReading = 1
Const ForAppending = 8
Const adOpenStatic = 3
Const adLockOptimistic = 3
Const adUseClient = 3

Set objConnection = CreateObject("ADODB.Connection")
Set objRecordset = CreateObject("ADODB.Recordset")

Set objArgs = WScript.Arguments
Set objFSO = CreateObject("Scripting.FileSystemObject")
Set objDictionary = CreateObject("Scripting.Dictionary")
Set objTextFile = objFSO.OpenTextFile(objArgs(0), ForReading)
i = 0
 
Do While objTextFile.AtEndOfStream <> True
  strNextLine = objTextFile.Readline
  objDictionary.Add i, strNextLine
  i = i + 1
Loop

objConnection.Open "DSN=Computer_Management;"

For Each objItem in objDictionary
On Error Resume Next
strcomputer = objDictionary.Item(objItem)
Set objWMIService = GetObject("winmgmts:" _
    & "{impersonationLevel=impersonate}!\\" & strComputer & "\root\cimv2")
Set colVolumes = objWMIService.ExecQuery("Select * from Win32_Volume")
objRecordset.CursorLocation = adUseClient
  objRecordset.Open "SELECT * FROM Win32_Volume " , objConnection, _
    adOpenStatic, adLockOptimistic 
For Each objVolume in colVolumes
    objRecordset.AddNew
    objRecordset("strcomputer") = strcomputer
    errResult = objVolume.DefragAnalysis(blnRecommended, objReport)
    If errResult = 0 Then
        objRecordset("objReport_AverageFileSize") = objReport.AverageFileSize
        objRecordset("objReport_AverageFragmentsPerFile") = objReport.AverageFragmentsPerFile
        objRecordset("objReport_ClusterSize") = objReport.ClusterSize
        objRecordset("objReport_ExcessFolderFragments") = objReport.ExcessFolderFragments
        objRecordset("objReport_FilePercentFragmentation") = objReport.FilePercentFragmentation
        objRecordset("objReport_FragmentedFolders") = objReport.FragmentedFolders
        objRecordset("objReport_FreeSpace") = objReport.FreeSpace
        objRecordset("objReport_FreeSpacePercent") = objReport.FreeSpacePercent
        objRecordset("objReport_FreeSpacePercentFragmentation") = objReport.FreeSpacePercentFragmentation
        objRecordset("objReport_MFTPercentInUse") = objReport.MFTPercentInUse
        objRecordset("objReport_MFTRecordCount") = objReport.MFTRecordCount
        objRecordset("objReport_PageFileSize") = objReport.PageFileSize
        objRecordset("objReport_TotalExcessFragments") =  objReport.TotalExcessFragments
        objRecordset("objReport_TotalFiles") = objReport.TotalFiles
        objRecordset("objReport_TotalFolders") = objReport.TotalFolders
        objRecordset("objReport_TotalFragmentedFile") = objReport.TotalFragmentedFiles
        objRecordset("objReport_TotalMFTFragments") = objReport.TotalMFTFragments
        objRecordset("objReport_TotalMFTSize") = objReport.TotalMFTSize
        objRecordset("objReport_TotalPageFileFragments") = objReport.TotalPageFileFragments
        objRecordset("objReport_TotalPercentFragmentation") = objReport.TotalPercentFragmentation
        objRecordset("objReport_UsedSpace") = objReport.UsedSpace
        objRecordset("objReport_VolumeName") = objReport.VolumeName
        objRecordset("objReport_VolumeSize") = objReport.VolumeSize       
        If blnRecommended = True Then
           objRecordset("blnRecommended") = YES
        Else
           objRecordset("blnRecommended") = NO
        End If
    End IF
    objRecordset.Update
  Next
Next
objRecordset.Close
objConnection.Close

Set objArgs = nothing
Set objFSO = nothing
Set objDictionary = nothing
Set objTextFile = nothing
Set objLogFile = nothing
Set objWMIService = nothing