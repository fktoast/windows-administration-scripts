Const ForReading = 1
Const ForAppending = 8
Const adOpenStatic = 3
Const adLockOptimistic = 3
Const adUseClient = 3

Set objConnection = CreateObject("ADODB.Connection")
Set objRecordset = CreateObject("ADODB.Recordset")

Set objArgs = WScript.Arguments
Set objFSO = CreateObject("Scripting.FileSystemObject")
Set objDictionary = CreateObject("Scripting.Dictionary")
Set objTextFile = objFSO.OpenTextFile(objArgs(0), ForReading)
i = 0
 
Do While objTextFile.AtEndOfStream <> True
  strNextLine = objTextFile.Readline
  objDictionary.Add i, strNextLine
  i = i + 1
Loop

objConnection.Open "DSN=Computer_Management;"

For Each objItem in objDictionary
On Error Resume Next
strcomputer = objDictionary.Item(objItem)
Set objWMIService = GetObject("winmgmts:\\" & strComputer & "\root\cimv2")
Set colItems = objWMIService.ExecQuery("Select * from Win32_POTSModem")
objRecordset.CursorLocation = adUseClient
  objRecordset.Open "SELECT * FROM Win32_POTSModem " , objConnection, _
    adOpenStatic, adLockOptimistic
objRecordset.AddNew 
For Each objItem2 in colItems
    
    objRecordset("strcomputer") = strcomputer
    objRecordSet("objItem_AttachedTo") = objItem2.AttachedTo
    objRecordSet("objItem_BlindOff") = objItem2.BlindOff
    objRecordSet("objItem_BlindOn") = objItem2.BlindOn
    objRecordSet("objItem_CompressionOff") = objItem2.CompressionOff
    objRecordSet("objItem_CompressionOn") = objItem2.CompressionOn
    objRecordSet("objItem_ConfigManagerErrorCode") = objItem2.ConfigManagerErrorCode
    objRecordSet("objItem_ConfigManagerUserConfig") = objItem2.ConfigManagerUserConfig
    objRecordSet("objItem_ConfigurationDialog") = objItem2.ConfigurationDialog
    objRecordSet("objItem_CountrySelected") = objItem2.CountrySelected
    objRecordset.Update
    For Each objElement1 In objItem2.DCB
        
        objRecordset("strcomputer") = strcomputer
        objRecordSet("objItem_DCB") = objElement1
        objRecordset.Update
    Next
    objRecordSet("") =  "Default: "
    For Each objElement2 In objItem2.Default
      
      
      objRecordSet("objItem_Default") =  objElement2
      objRecordset.Update
    Next
    
    
    objRecordSet("objItem_DeviceID") = objItem2.DeviceID
    objRecordSet("objItem_DeviceType") = objItem2.DeviceType
    objRecordSet("objItem_DriverDate") = objItem2.DriverDate
    objRecordSet("objItem_ErrorControlForced") = objItem2.ErrorControlForced
    objRecordSet("objItem_ErrorControlOff") = objItem2.ErrorControlOff
    objRecordSet("objItem_ErrorControlOn") = objItem2.ErrorControlOn
    objRecordSet("objItem_FlowControlHard") = objItem2.FlowControlHard
    objRecordSet("objItem_FlowControlOff") = objItem2.FlowControlOff
    objRecordSet("objItem_FlowControlSoft") = objItem2.FlowControlSoft
    objRecordSet("objItem_InactivityScale") = objItem2.InactivityScale
    objRecordSet("objItem_InactivityTimeout") = objItem2.InactivityTimeout
    objRecordSet("objItem_Index") = objItem2.Index
    objRecordSet("objItem_MaxBaudRateToSerialPort") = objItem2.MaxBaudRateToSerialPort
    objRecordSet("objItem_Model") = objItem2.Model
    objRecordSet("objItem_ModemInfPath") = objItem2.ModemInfPath
    objRecordSet("objItem_ModemInfSection") = objItem2.ModemInfSection
    objRecordSet("objItem_ModulationBell") = objItem.ModulationBell
    objRecordSet("objItem_ModulationCCITT") =  objItem2.ModulationCCITT
    objRecordSet("objItem_Name") = objItem2.Name
    objRecordSet("objItem_PNPDeviceID") = objItem.PNPDeviceID
    objRecordSet("objItem_PortSubClass") = objItem.PortSubClass
    objRecordSet("objItem_Prefix") = objItem2.Prefix
    objRecordset.Update
    For Each objElement3 In objItem2.Properties
        
        objRecordSet("objItem_Properties") = objElement3
        objRecordset.Update
    Next
    
    objRecordSet("objItem_ProviderName") = objItem2.ProviderName
    objRecordSet("objItem_Pulse") = objItem2.Pulse
    objRecordSet("objItem_Resest") = objItem2.Reset
    objRecordSet("objItem_ResponsesKeyName") = objItem2.ResponsesKeyName
    objRecordSet("objItem_SpeakerModeDial") = objItem2.SpeakerModeDial
    objRecordSet("objItem_SpeakerModeOff") = objItem2.SpeakerModeOff
    objRecordSet("objItem_SpeakerModeOn") = objItem2.SpeakerModeOn
    objRecordSet("objItem_SpeakerModeSetup") = objItem2.SpeakerModeSetup
    objRecordSet("objItem_SpeakerVolumeHigh") = objItem2.SpeakerVolumeHigh
    objRecordSet("objItem_SpeakerVolumeInfo") = objItem2.SpeakerVolumeInfo
    objRecordSet("objItem_SpeakerVolumeLow") = objItem2.SpeakerVolumeLow
    objRecordSet("objItem_SpeakerVolumeMed") = objItem2.SpeakerVolumeMed
    objRecordSet("objItem_StatusInfo") = objItem2.StatusInfo
    objRecordSet("objItem_Terminator") = objItem2.Terminator
    objRecordSet("objItem_Tone") = objItem2.Tone
    objRecordSet.Update
Next
Next

objRecordset.Close
objConnection.Close

Set objArgs = nothing
Set objFSO = nothing
Set objDictionary = nothing
Set objTextFile = nothing
Set objLogFile = nothing
Set objWMIService = nothing