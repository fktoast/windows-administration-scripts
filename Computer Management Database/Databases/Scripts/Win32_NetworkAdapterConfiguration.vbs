Const ForReading = 1
Const ForAppending = 8
Const adOpenStatic = 3
Const adLockOptimistic = 3
Const adUseClient = 3

Set objConnection = CreateObject("ADODB.Connection")
Set objRecordset = CreateObject("ADODB.Recordset")

Set objArgs = WScript.Arguments
Set objFSO = CreateObject("Scripting.FileSystemObject")
Set objDictionary = CreateObject("Scripting.Dictionary")
Set objTextFile = objFSO.OpenTextFile(objArgs(0), ForReading)
i = 0
 
Do While objTextFile.AtEndOfStream <> True
  strNextLine = objTextFile.Readline
  objDictionary.Add i, strNextLine
  i = i + 1
Loop

objConnection.Open "DSN=Computer_Management;"

For Each objItem in objDictionary
  On Error Resume Next
  strcomputer = objDictionary.Item(objItem)
  Set objWMIService = GetObject("winmgmts:" _
    & "{impersonationLevel=impersonate}!\\" & strComputer & "\root\cimv2")
  Set colItems2 = objWMIService.ExecQuery _
    ("Select * from Win32_NetworkAdapterConfiguration")
  objRecordset.CursorLocation = adUseClient
  objRecordset.Open "SELECT * FROM Win32_NetworkAdapterConfiguration " , objConnection, _
    adOpenStatic, adLockOptimistic 
  For Each objItem2 in colItems2
    objRecordset.AddNew
    objRecordset("strcomputer") = strcomputer
    objRecordset("objItem_ArpAlwaysSourceRoute") = objItem2.ArpAlwaysSourceRoute
    objRecordset("objItem_ArpUseEtherSNAP") = objItem2.ArpUseEtherSNAP
    objRecordset("objItem_Caption") = objItem2.Caption
    objRecordset("objItem_DatabasePath") = objItem2.DatabasePath
    objRecordset("objItem_DeadGWDetectEnabled") = objItem2.DeadGWDetectEnabled
    objRecordset("objItem_DefaultIPGateway") = objItem2.DefaultIPGateway
    objRecordset("objItem_DefaultTOS") = objItem2.DefaultTOS
    objRecordset("objItem_DefaultTTL") = objItem2.DefaultTTL
    objRecordset("objItem_Description") = objItem2.Description
    objRecordset("objItem_DHCPEnabled") = objItem2.DHCPEnabled
    objRecordset("objItem_DHCPLeaseExpires") = objItem2.DHCPLeaseExpires
    objRecordset("objItem_DHCPLeaseObtained") = objItem2.DHCPLeaseObtained
    objRecordset("objItem_DHCPServer") = objItem2.DHCPServer
    objRecordset("objItem_DNSDomain") = objItem2.DNSDomain
    objRecordset("objItem_DNSDomainSuffixSearchOrder") = objItem2.DNSDomainSuffixSearchOrder
    objRecordset("objItem_DNSEnabledForWINSResolution") = objItem2.DNSEnabledForWINSResolution
    objRecordset("objItem_DNSHostName") = objItem2.DNSHostName
    objRecordset("objItem_DNSServerSearchOrder") = objItem2.DNSServerSearchOrder
    objRecordset("objItem_DomainDNSRegistrationEnabled") = objItem2.DomainDNSRegistrationEnabled
    objRecordset("objItem_ForwardBufferMemory") = objItem2.ForwardBufferMemory
    objRecordset("objItem_FullDNSRegistrationEnabled") = objItem2.FullDNSRegistrationEnabled
    objRecordset("objItem_GatewayCostMetric") = objItem2.GatewayCostMetric
    objRecordset("objItem_IGMPLevel") = objItem2.IGMPLevel
    objRecordset("objItem_Index") = objItem2.Index
    objRecordset("objItem_IPAddress") = objItem2.IPAddress
    objRecordset("objItem_IPConnectionMetric") = objItem2.IPConnectionMetric
    objRecordset("objItem_IPEnabled") = objItem2.IPEnabled
    objRecordset("objItem_IPFilterSecurityEnabled") = objItem2.IPFilterSecurityEnabled
    objRecordset("objItem_IPPortSecurityEnabled") = objItem2.IPPortSecurityEnabled
    objRecordset("objItem_IPSecPermitIPProtocols") = objItem2.IPSecPermitIPProtocols
    objRecordset("objItem_IPSecPermitTCPPorts") = objItem2.IPSecPermitTCPPorts
    objRecordset("objItem_IPSecPermitUDPPorts") = objItem2.IPSecPermitUDPPorts
    objRecordset("objItem_IPSubnet") = objItem2.IPSubnet
    objRecordset("objItem_IPUseZeroBroadcast") = objItem2.IPUseZeroBroadcast
    objRecordset("objItem_IPXAddress") = objItem2.IPXAddress
    objRecordset("objItem_IPXEnabled") = objItem2.IPXEnabled
    objRecordset("objItem_IPXFrameType") = objItem2.IPXFrameType
    objRecordset("objItem_IPXMediaType") = objItem2.IPXMediaType
    objRecordset("objItem_IPXNetworkNumber") = objItem2.IPXNetworkNumber
    objRecordset("objItem_IPXVirtualNetNumber") = objItem2.IPXVirtualNetNumber
    objRecordset("objItem_KeepAliveInterval") = objItem2.KeepAliveInterval
    objRecordset("objItem_KeepAliveTime") = objItem2.KeepAliveTime
    objRecordset("objItem_MACAddress") = objItem2.MACAddress
    objRecordset("objItem_MTU") = objItem2.MTU
    objRecordset("objItem_NumForwardPackets") = objItem2.NumForwardPackets
    objRecordset("objItem_PMTUBHDetectEnabled") = objItem2.PMTUBHDetectEnabled
    objRecordset("objItem_PMTUDiscoveryEnabled") = objItem2.PMTUDiscoveryEnabled
    objRecordset("objItem_ServiceName") = objItem2.ServiceName
    objRecordset("objItem_SettingID") = objItem2.SettingID
    objRecordset("objItem_TcpipNetbiosOptions") = objItem2.TcpipNetbiosOptions
    objRecordset("objItem_TcpMaxConnectRetransmissions") = objItem2.TcpMaxConnectRetransmissions
    objRecordset("objItem_TcpMaxDataRetransmissions") = objItem2.TcpMaxDataRetransmissions
    objRecordset("objItem_TcpNumConnections") = objItem2.TcpNumConnections
    objRecordset("objItem_TcpUseRFC112UrgentPointer") = objItem2.TcpUseRFC112UrgentPointer
    objRecordset("objItem_TcpWindowSize") = objItem2.TcpWindowSize
    objRecordset("objItem_WINSEnableLMHostsLookup") = objItem2.WINSEnableLMHostsLookup
    objRecordset("objItem_WINSHostLookupFile") = objItem2.WINSHostLookupFile
    objRecordset("objItem_WINSPrimaryServer") = objItem2.WINSPrimaryServer
    objRecordset("objItem_WINSScopeID") = objItem2.WINSScopeID
    objRecordset("objItem_WINSSecondaryServer") = objItem2.WINSSecondaryServer
    objRecordset.Update
  Next
Next

objRecordset.Close
objConnection.Close

Set objArgs = nothing
Set objFSO = nothing
Set objDictionary = nothing
Set objTextFile = nothing
Set objLogFile = nothing
Set objWMIService = nothing