Const ForReading = 1
Const ForAppending = 8
Const adOpenStatic = 3
Const adLockOptimistic = 3
Const adUseClient = 3

Set objConnection = CreateObject("ADODB.Connection")
Set objRecordset = CreateObject("ADODB.Recordset")

Set objArgs = WScript.Arguments
Set objFSO = CreateObject("Scripting.FileSystemObject")
Set objDictionary = CreateObject("Scripting.Dictionary")
Set objTextFile = objFSO.OpenTextFile(objArgs(0), ForReading)
i = 0
 
Do While objTextFile.AtEndOfStream <> True
  strNextLine = objTextFile.Readline
  objDictionary.Add i, strNextLine
  i = i + 1
Loop

objConnection.Open "DSN=Computer_Management;"

For Each objItem in objDictionary
  On Error Resume Next
  strcomputer = objDictionary.Item(objItem)
  Set objWMIService = GetObject("winmgmts:\\" & strComputer & "\root\cimv2")
  Set colItems = objWMIService.ExecQuery _
    ("Select * from Win32_SerialPortConfiguration")
  objRecordset.CursorLocation = adUseClient
  objRecordset.Open "SELECT * FROM Win32_SerialPortConfiguration " , objConnection, _
    adOpenStatic, adLockOptimistic 
  For Each objItem2 in colItems
    objRecordset.AddNew
    objRecordset("strcomputer") = strcomputer
    objRecordset("objItem_AbortReadWriteOnError") = objItem2.AbortReadWriteOnError
    objRecordset("objItem_BaudRate") = objItem2.BaudRate
    objRecordset("objItem_BinaryModeEnabled") = objItem2.BinaryModeEnabled
    objRecordset("objItem_BitsPerByte") = objItem2.BitsPerByte
    objRecordset("objItem_ContinueXMitOnXOff") = objItem2.ContinueXMitOnXOff
    objRecordset("objItem_CTSOutflowControl") = objItem2.CTSOutflowControl
    objRecordset("objItem_DiscardNULLBytes") = objItem2.DiscardNULLBytes
    objRecordset("objItem_DSROutflowControl") = objItem2.DSROutflowControl
    objRecordset("objItem_DSRSensitivity") = objItem2.DSRSensitivity
    objRecordset("objItem_DTRFlowControlType") = objItem2.DTRFlowControlType
    objRecordset("objItem_EOFCharacter") = objItem2.EOFCharacter
    objRecordset("objItem_ErrorReplaceCharacter") = objItem2.ErrorReplaceCharacter
    objRecordset("objItem_ErrorReplacementEnabled") = objItem2.ErrorReplacementEnabled
    objRecordset("objItem_EventCharacter") = objItem2.EventCharacter
    objRecordset("objItem_IsBusy") = objItem2.IsBusy
    objRecordset("objItem_Name") = objItem2.Name
    objRecordset("objItem_Parity") = objItem2.Parity
    objRecordset("objItem_ParityCheckEnabled") = objItem2.ParityCheckEnabled
    objRecordset("objItem_RTSFlowControlType") = objItem2.RTSFlowControlType
    objRecordset("objItem_SettingID") = objItem2.SettingID
    objRecordset("objItem_StopBits") = objItem2.StopBits
    objRecordset("objItem_XOffCharacter") = objItem2.XOffCharacter
    objRecordset("objItem_XOffXMitThreshold") = objItem2.XOffXMitThreshold
    objRecordset("objItem_XOnCharacter") = objItem2.XOnCharacter
    objRecordset("objItem_XOnXMitThreshold") = objItem2.XOnXMitThreshold
    objRecordset("objItem_XOnXOffInFlowControl") = objItem2.XOnXOffInFlowControl
    objRecordset("objItem_XOnXOffOutFlowControl") = objItem2.XOnXOffOutFlowControl
    objRecordset.Update
  Next
Next

objRecordset.Close
objConnection.Close

Set objArgs = nothing
Set objFSO = nothing
Set objDictionary = nothing
Set objTextFile = nothing
Set objLogFile = nothing
Set objWMIService = nothing
  