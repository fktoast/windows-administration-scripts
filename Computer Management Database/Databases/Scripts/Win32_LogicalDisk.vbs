Const ForReading = 1
Const ForAppending = 8
Const adOpenStatic = 3
Const adLockOptimistic = 3
Const adUseClient = 3

Set objConnection = CreateObject("ADODB.Connection")
Set objRecordset = CreateObject("ADODB.Recordset")

Set objArgs = WScript.Arguments
Set objFSO = CreateObject("Scripting.FileSystemObject")
Set objDictionary = CreateObject("Scripting.Dictionary")
Set objTextFile = objFSO.OpenTextFile(objArgs(0), ForReading)
i = 0
 
Do While objTextFile.AtEndOfStream <> True
  strNextLine = objTextFile.Readline
  objDictionary.Add i, strNextLine
  i = i + 1
Loop

objConnection.Open "DSN=Computer_Management;"

For Each objItem in objDictionary
On Error Resume Next
strcomputer = objDictionary.Item(objItem)
Set objWMIService = GetObject("winmgmts:" _
    & "{impersonationLevel=impersonate}!\\" & strComputer & "\root\cimv2")
Set colDisks = objWMIService.ExecQuery _
    ("Select * from Win32_LogicalDisk")
objRecordset.CursorLocation = adUseClient
  objRecordset.Open "SELECT * FROM Win32_LogicalDisk " , objConnection, _
    adOpenStatic, adLockOptimistic
  For each objDisk in colDisks
    objRecordset.AddNew
    objRecordset("strcomputer") = strcomputer
    objRecordset("objDisk_Compressed") = objDisk.Compressed  
    objRecordset("objDisk_Description") = objDisk.Description       
    objRecordset("objDisk_DeviceID") = objDisk.DeviceID      
    objRecordset("objDisk_DriveType") = objDisk.DriveType    
    objRecordset("objDisk_FileSystem") = objDisk.FileSystem  
    objRecordset("objDisk_FreeSpace") = objDisk.FreeSpace    
    objRecordset("objDisk_MediaType") = objDisk.MediaType    
    objRecordset("objDisk_Name") = objDisk.Name      
    objRecordset("objDisk_QuotasDisabled") = objDisk.QuotasDisabled
    objRecordset("objDisk_QuotasIncomplete") = objDisk.QuotasIncomplete
    objRecordset("objDisk_QuotasRebuilding") = objDisk.QuotasRebuilding
    objRecordset("objDisk_Size") = objDisk.Size      
    objRecordset("objDisk_SupportsDiskQuotas") = objDisk.SupportsDiskQuotas      
    objRecordset("objDisk_SupportsFileBasedCompression") = objDisk.SupportsFileBasedCompression   
    objRecordset("objDisk_SystemName") = objDisk.SystemName  
    objRecordset("objDisk_VolumeDirty") = objDisk.VolumeDirty       
    objRecordset("objDisk_VolumeName") = objDisk.VolumeName  
    objRecordset("objDisk_VolumeSerialNumber") = objDisk.VolumeSerialNumber
    objRecordset.Update      
  Next
Next
objRecordset.Close
objConnection.Close

Set objArgs = nothing
Set objFSO = nothing
Set objDictionary = nothing
Set objTextFile = nothing
Set objLogFile = nothing
Set objWMIService = nothing