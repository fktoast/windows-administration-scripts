Const ForReading = 1
Const ForAppending = 8
Const adOpenStatic = 3
Const adLockOptimistic = 3
Const adUseClient = 3

Set objConnection = CreateObject("ADODB.Connection")
Set objRecordset = CreateObject("ADODB.Recordset")

Set objArgs = WScript.Arguments
Set objFSO = CreateObject("Scripting.FileSystemObject")
Set objDictionary = CreateObject("Scripting.Dictionary")
Set objTextFile = objFSO.OpenTextFile(objArgs(0), ForReading)
i = 0
 
Do While objTextFile.AtEndOfStream <> True
  strNextLine = objTextFile.Readline
  objDictionary.Add i, strNextLine
  i = i + 1
Loop

objConnection.Open "DSN=Computer_Management;"

For Each objItem in objDictionary
  On Error Resume Next
  strcomputer = objDictionary.Item(objItem)
  Set objWMIService = GetObject("winmgmts:\\" & strComputer & "\root\cimv2")
  Set colItems = objWMIService.ExecQuery("Select * from Win32_PointingDevice")
  objRecordset.CursorLocation = adUseClient
  objRecordset.Open "SELECT * FROM Win32_PointingDevice " , objConnection, _
    adOpenStatic, adLockOptimistic 
  For Each objItem2 in colItems
    objRecordset.AddNew
    objRecordset("strcomputer") = strcomputer
    objRecordset("objItem_Description") = objItem2.Description
    objRecordset("objItem_DeviceID") = objItem2.DeviceID
    objRecordset("objItem_DeviceInterface") = objItem2.DeviceInterface
    objRecordset("objItem_DoubleSpeedThreshold") = objItem2.DoubleSpeedThreshold
    objRecordset("objItem_Handedness") = objItem2.Handedness
    objRecordset("objItem_HardwareType") = objItem2.HardwareType
    objRecordset("objItem_InfFileName") = objItem2.InfFileName
    objRecordset("objItem_InfSection") = objItem2.InfSection
    objRecordset("objItem_Manufacturer") = objItem2.Manufacturer
    objRecordset("objItem_Name") = objItem2.Name
    objRecordset("objItem_NumberOfButtons") = objItem2.NumberOfButtons
    objRecordset("objItem_PNPDeviceID") = objItem2.PNPDeviceID
    objRecordset("objItem_PointingType") = objItem2.PointingType
    objRecordset("objItem_QuadSpeedThreshold") = objItem2.QuadSpeedThreshold
    objRecordset("objItem_Resolution") = objItem2.Resolution
    objRecordset("objItem_SampleRate") = objItem2.SampleRate
    objRecordset("objItem_Synch") = objItem2.Synch
    objRecordset.Update
  Next
Next

objRecordset.Close
objConnection.Close

Set objArgs = nothing
Set objFSO = nothing
Set objDictionary = nothing
Set objTextFile = nothing
Set objLogFile = nothing
Set objWMIService = nothing