Const ForReading = 1
Const ForAppending = 8
Const adOpenStatic = 3
Const adLockOptimistic = 3
Const adUseClient = 3

Set objConnection = CreateObject("ADODB.Connection")
Set objRecordset = CreateObject("ADODB.Recordset")

Set objArgs = WScript.Arguments
Set objFSO = CreateObject("Scripting.FileSystemObject")
Set objDictionary = CreateObject("Scripting.Dictionary")
Set objTextFile = objFSO.OpenTextFile(objArgs(0), ForReading)
i = 0
 
Do While objTextFile.AtEndOfStream <> True
  strNextLine = objTextFile.Readline
  objDictionary.Add i, strNextLine
  i = i + 1
Loop

objConnection.Open "DSN=Computer_Management;"

For Each objItem in objDictionary
On Error Resume Next
strcomputer = objDictionary.Item(objItem)
Set objWMIService = GetObject("winmgmts:\\" & strComputer & "\root\cimv2")
Set colItems = objWMIService.ExecQuery _
    ("Select * from Win32_DisplayControllerConfiguration")
objRecordset.CursorLocation = adUseClient
  objRecordset.Open "SELECT * FROM Win32_DisplayControllerConfiguration " , objConnection, _
    adOpenStatic, adLockOptimistic 
For Each objItem2 in colItems
    objRecordset.AddNew
    objRecordset("strcomputer") = strcomputer
    objRecordset("objItem_BitsPerPixel") = objItem2.BitsPerPixel
    objRecordset("objItem_ColorPlanes") = objItem2.ColorPlanes
    objRecordset("objItem_DeviceEntriesInAColorTable") = objItem2.DeviceEntriesInAColorTable
    objRecordset("objItem_DeviceSpecificPens") = objItem2.DeviceSpecificPens
    objRecordset("objItem_HorizontalResolution") = objItem2.HorizontalResolution
    objRecordset("objItem_Name") = objItem2.Name
    objRecordset("objItem_RefreshRate") = objItem2.RefreshRate
    objRecordset("objItem_SettingID") = objItem2.SettingID
    objRecordset("objItem_VerticalResolution") = objItem2.VerticalResolution
    objRecordset("objItem_VideoMode") = objItem2.VideoMode
    objRecordset.Update

Next
Next

objRecordset.Close
objConnection.Close

Set objArgs = nothing
Set objFSO = nothing
Set objDictionary = nothing
Set objTextFile = nothing
Set objLogFile = nothing
Set objWMIService = nothing