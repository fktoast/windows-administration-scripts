Const ForReading = 1
Const ForAppending = 8
Const adOpenStatic = 3
Const adLockOptimistic = 3
Const adUseClient = 3

Set objConnection = CreateObject("ADODB.Connection")
Set objRecordset = CreateObject("ADODB.Recordset")

Set objArgs = WScript.Arguments
Set objFSO = CreateObject("Scripting.FileSystemObject")
Set objDictionary = CreateObject("Scripting.Dictionary")
Set objTextFile = objFSO.OpenTextFile(objArgs(0), ForReading)
i = 0
 
Do While objTextFile.AtEndOfStream <> True
  strNextLine = objTextFile.Readline
  objDictionary.Add i, strNextLine
  i = i + 1
Loop

objConnection.Open "DSN=Computer_Management;"

For Each objItem in objDictionary
  On Error Resume Next
  strcomputer = objDictionary.Item(objItem)
  Set objWMIService = GetObject("winmgmts:\\" & strComputer & "\root\cimv2")
  Set colItems = objWMIService.ExecQuery _
    ("Select * from Win32_SerialPortConfiguration")
  objRecordset.CursorLocation = adUseClient
  Set objWMIService = GetObject("winmgmts:\\" & strComputer & "\root\cimv2")
  Set colItems = objWMIService.ExecQuery("Select * from Win32_SerialPort",,48)
  objRecordset.CursorLocation = adUseClient
  objRecordset.Open "SELECT * FROM Win32_SerialPort " , objConnection, _
    adOpenStatic, adLockOptimistic 
  For Each objItem3 in colItems
    objRecordset.AddNew
    objRecordset("strComputer") = strComputer
    objRecordset("objItem_Binary") = objItem3.Binary
    objRecordset("objItem_Description") = objItem3.Description
    objRecordset("objItem_DeviceID") = objItem3.DeviceID
    objRecordset("objItem_MaxBaudRate") = objItem3.MaxBaudRate
    objRecordset("objItem_MaximumInputBufferSize") = objItem3.MaximumInputBufferSize
    objRecordset("objItem_MaximumOutputBufferSize") = objItem3.MaximumOutputBufferSize
    objRecordset("objItem_Name") = objItem3.Name
    objRecordset("objItem_OSAutoDiscovered") = objItem3.OSAutoDiscovered
    objRecordset("objItem_PNPDeviceID") = objItem3.PNPDeviceID
    objRecordset("objItem_ProviderType") = objItem3.ProviderType
    objRecordset("objItem_SettableBaudRate") = objItem3.SettableBaudRate
    objRecordset("objItem_SettableDataBits") = objItem3.SettableDataBits
    objRecordset("objItem_SettableFlowControl") = objItem3.SettableFlowControl
    objRecordset("objItem_SettableParity") = objItem3.SettableParity
    objRecordset("objItem_SettableParityCheck") = objItem3.SettableParityCheck
    objRecordset("objItem_SettableRLSD") = objItem3.SettableRLSD
    objRecordset("objItem_SettableStopBits") = objItem3.SettableStopBits
    objRecordset("objItem_Supports16BitMode") = objItem3.Supports16BitMode
    objRecordset("objItem_SupportsDTRDSR") = objItem3.SupportsDTRDSR
    objRecordset("objItem_SupportsElapsedTimeouts") = objItem3.SupportsElapsedTimeouts
    objRecordset("objItem_SupportsIntTimeouts") = objItem3.SupportsIntTimeouts
    objRecordset("objItem_SupportsParityCheck") = objItem3.SupportsParityCheck
    objRecordset("objItem_SupportsRLSD") = objItem3.SupportsRLSD
    objRecordset("objItem_SupportsRTSCTS") = objItem3.SupportsRTSCTS
    objRecordset("objItem_SupportsSpecialCharacters") = objItem3.SupportsSpecialCharacters
    objRecordset("objItem_SupportsXOnXOff") = objItem3.SupportsXOnXOff
    objRecordset("objItem_SupportsXOnXOffSet") = objItem3.SupportsXOnXOffSet
    objRecordset.Update
  Next
Next

objRecordset.Close
objConnection.Close

Set objArgs = nothing
Set objFSO = nothing
Set objDictionary = nothing
Set objTextFile = nothing
Set objLogFile = nothing
Set objWMIService = nothing