Const ForReading = 1
Const ForAppending = 8
Const adOpenStatic = 3
Const adLockOptimistic = 3
Const adUseClient = 3

Set objConnection = CreateObject("ADODB.Connection")
Set objRecordset = CreateObject("ADODB.Recordset")

Set objArgs = WScript.Arguments
Set objFSO = CreateObject("Scripting.FileSystemObject")
Set objDictionary = CreateObject("Scripting.Dictionary")
Set objTextFile = objFSO.OpenTextFile(objArgs(0), ForReading)
i = 0
 
Do While objTextFile.AtEndOfStream <> True
  strNextLine = objTextFile.Readline
  objDictionary.Add i, strNextLine
  i = i + 1
Loop

objConnection.Open "DSN=Computer_Management;"

For Each objItem in objDictionary
    strcomputer = objDictionary.Item(objItem)
    Set objWMIService = GetObject("winmgmts:" _
        & "{impersonationLevel=impersonate}!\\" & strComputer & "\root\cimv2")
    Set colBIOS = objWMIService.ExecQuery _
        ("Select * from Win32_BIOS")
    objRecordset.CursorLocation = adUseClient
        objRecordset.Open "SELECT * FROM Win32_BIOS" , objConnection, _
        adOpenStatic, adLockOptimistic
    For each objBIOS in colBIOS
        objRecordset.AddNew
        objRecordset("strcomputer") = strcomputer
        objRecordset("objBIOS_Version") = objBIOS.Version
        objRecordset("objBIOS_BuildNumber") = objBIOS.BuildNumber
        objRecordset("objBIOS_CurrentLanguage") = objBIOS.CurrentLanguage
        objRecordset("objBIOS_InstallableLanguages") = objBIOS.InstallableLanguages
        objRecordset("objBIOS_Manufacturer") = objBIOS.Manufacturer
        objRecordset("objBIOS_Name") = objBIOS.Name
        objRecordset("objBIOS_PrimaryBIOS") = objBIOS.PrimaryBIOS
        objRecordset("objBIOS_ReleaseDate") = objBIOS.ReleaseDate
        objRecordset("objBIOS_SerialNumber") = objBIOS.SerialNumber
        objRecordset("objBIOS_SMBIOSBIOSVersion") = objBIOS.SMBIOSBIOSVersion
        objRecordset("objBIOS_SMBIOSMajorVersion") = objBIOS.SMBIOSMajorVersion
        objRecordset("objBIOS_SMBIOSMinorVersion") = objBIOS.SMBIOSMinorVersion
        objRecordset("objBIOS_SMBIOSPresent") = objBIOS.SMBIOSPresent
        objRecordset("objBIOS_Status") = objBIOS.Status
        objRecordset.Update
    Next
Next

objRecordset.Close
objConnection.Close

Set objArgs = nothing
Set objFSO = nothing
Set objDictionary = nothing
Set objTextFile = nothing
Set objLogFile = nothing
Set objWMIService = nothing
