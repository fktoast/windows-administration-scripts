Const ForReading = 1
Const ForAppending = 8
Const adOpenStatic = 3
Const adLockOptimistic = 3
Const adUseClient = 3

Set objConnection = CreateObject("ADODB.Connection")
Set objRecordset = CreateObject("ADODB.Recordset")

Set objArgs = WScript.Arguments
Set objFSO = CreateObject("Scripting.FileSystemObject")
Set objDictionary = CreateObject("Scripting.Dictionary")
Set objTextFile = objFSO.OpenTextFile(objArgs(0), ForReading)
i = 0
 
Do While objTextFile.AtEndOfStream <> True
  strNextLine = objTextFile.Readline
  objDictionary.Add i, strNextLine
  i = i + 1
Loop

objConnection.Open "DSN=Computer_Management;"

For Each objItem in objDictionary
  strcomputer = objDictionary.Item(objItem)
Set objWMIService = GetObject("winmgmts:" _
    & "{impersonationLevel=impersonate}!\\" & strComputer & "\root\cimv2")
  Set colSettings = objWMIService.ExecQuery _
    ("Select * from Win32_OperatingSystem")
  objRecordset.CursorLocation = adUseClient
  objRecordset.Open "SELECT * FROM Win32_OperatingSystem " , objConnection, _
    adOpenStatic, adLockOptimistic 
  For Each objOperatingSystem in colSettings
    ON ERROR RESUME NEXT
    objRecordset.AddNew
    objRecordset("strcomputer") = strcomputer 
    objRecordset("OperatingSystem_Name") = objOperatingSystem.Name
    objRecordset("OperatingSystem_Version") = objOperatingSystem.Version
    objRecordset("OperatingSystem_ServicePackMajorVersion") = objOperatingSystem.ServicePackMajorVersion
    objRecordset("OperatingSystem_ServicePackMinorVersion") = objOperatingSystem.ServicePackMinorVersion
    objRecordset("OperatingSystem_Manufacturer") = objOperatingSystem.Manufacturer
    objRecordset("OperatingSystem_WindowsDirectory") = objOperatingSystem.WindowsDirectory
    objRecordset("OperatingSystem_Locale") = objOperatingSystem.Locale
    objRecordset("OperatingSystem_FreePhysicalMemory") = objOperatingSystem.FreePhysicalMemory
    objRecordset("OperatingSystem_TotalVirtualMemorySize") = objOperatingSystem.TotalVirtualMemorySize
    objRecordset("OperatingSystem_FreeVirtualMemory") = objOperatingSystem.FreeVirtualMemory
    objRecordset("OperatingSystem_SizeStoredInPagingFiles") = objOperatingSystem.SizeStoredInPagingFiles
    objRecordset.Update
  Next
Next

objRecordset.Close
objConnection.Close

Set objArgs = nothing
Set objFSO = nothing
Set objDictionary = nothing
Set objTextFile = nothing
Set objLogFile = nothing
Set objWMIService = nothing