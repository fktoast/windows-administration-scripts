Const ForReading = 1
Const ForAppending = 8
Const adOpenStatic = 3
Const adLockOptimistic = 3
Const adUseClient = 3

Set objConnection = CreateObject("ADODB.Connection")
Set objRecordset = CreateObject("ADODB.Recordset")

Set objArgs = WScript.Arguments
Set objFSO = CreateObject("Scripting.FileSystemObject")
Set objDictionary = CreateObject("Scripting.Dictionary")
Set objTextFile = objFSO.OpenTextFile(objArgs(0), ForReading)
i = 0
 
Do While objTextFile.AtEndOfStream <> True
  strNextLine = objTextFile.Readline
  objDictionary.Add i, strNextLine
  i = i + 1
Loop

objConnection.Open "DSN=Computer_Management;"

For Each objItem in objDictionary
  ON ERROR RESUME NEXT
  strcomputer = objDictionary.Item(objItem)
  Set colServices = GetObject("winmgmts:" _
    & "{impersonationLevel=impersonate}!//" & objDictionary.Item(objItem) _
    & "").ExecQuery("Select * from Win32_Service")
  objRecordset.CursorLocation = adUseClient
  objRecordset.Open "SELECT * FROM Win32_Service " , objConnection, _
    adOpenStatic, adLockOptimistic   
  Set objWMIService = GetObject("winmgmts:" _
    & "{impersonationLevel=impersonate}!\\" & strComputer & "\root\cimv2")
  Set colListOfServices = objWMIService.ExecQuery _
    ("Select * from Win32_Service")
  For Each objService in colListOfServices
    ON ERROR RESUME NEXT
    objRecordset.AddNew
    objRecordset("strcomputer") = strcomputer
    objRecordset("Service_Count") = colServices.Count 
    objRecordset("Service_Name") = objService.Name 
    objRecordset("Service_Type") = objService.ServiceType 
    objRecordset("Service_State") = objService.State 
    objRecordset("Exit_Code") = objService.ExitCode 
    objRecordset("Process_ID") = objService.ProcessID 
    objRecordset("Can_Be_Paused") = objService.AcceptPause 
    objRecordset("Can_Be_Stopped") = objService.AcceptStop 
    objRecordset("Caption") = objService.Caption 
    objRecordset("Description") = objService.Description 
    objRecordset("Can_Interact_with_Desktop") = objService.DesktopInteract 
    objRecordset("Display_Name") = objService.DisplayName 
    objRecordset("Error_Control") = objService.ErrorControl 
    objRecordset("Executable_Path_Name") = objService.PathName 
    objRecordset("Service_Started")= objService.Started 
    objRecordset("Start_Mode") = objService.StartMode 
    objRecordset("Account_Name") = objService.StartName
    objRecordset.Update
  Next
Next

objRecordset.Close
objConnection.Close

Set objArgs = nothing
Set objFSO = nothing
Set objDictionary = nothing
Set objTextFile = nothing
Set objLogFile = nothing
Set objWMIService = nothing