Const ForReading = 1
Const ForAppending = 8
Const adOpenStatic = 3
Const adLockOptimistic = 3
Const adUseClient = 3

Set objConnection = CreateObject("ADODB.Connection")
Set objRecordset = CreateObject("ADODB.Recordset")

Set objArgs = WScript.Arguments
Set objFSO = CreateObject("Scripting.FileSystemObject")
Set objDictionary = CreateObject("Scripting.Dictionary")
Set objTextFile = objFSO.OpenTextFile(objArgs(0), ForReading)
i = 0
 
Do While objTextFile.AtEndOfStream <> True
  strNextLine = objTextFile.Readline
  objDictionary.Add i, strNextLine
  i = i + 1
Loop

objConnection.Open "DSN=Computer_Management;"

For Each objItem in objDictionary
    On Error Resume Next
    strcomputer = objDictionary.Item(objItem)
    Set objFSO = CreateObject("Scripting.FileSystemObject")
    objRecordset.CursorLocation = adUseClient
    objRecordset.Open "SELECT * FROM Win32_DiskDriveStatistics " , objConnection, _
        adOpenStatic, adLockOptimistic
    Set colDrives = objFSO.Drives
    For Each objDrive in colDrives
        objRecordset.AddNew
        objRecordset("strcomputer") = strcomputer
        objRecordset("objDrive_AvailableSpace") = objDrive.AvailableSpace
        objRecordset("objDrive_DriveLetter") = objDrive.DriveLetter
        objRecordset("objDrive_DriveType") = objDrive.DriveType
        objRecordset("objDrive_FileSystem") = objDrive.FileSystem
        objRecordset("objDrive_FreeSpace") = objDrive.FreeSpace
        objRecordset("objDrive_IsReady") = objDrive.IsReady
        objRecordset("objDrive_Path") = objDrive.Path
        objRecordset("objDrive_RootFolder") = objDrive.RootFolder
        objRecordset("objDrive_SerialNumber") = objDrive.SerialNumber
        objRecordset("objDrive_ShareName") = objDrive.ShareName
        objRecordset("objDrive_TotalSize") = objDrive.TotalSize
        objRecordset("objDrive_VolumeName") = objDrive.VolumeName
        objRecordset.Update
    Next
Next

objRecordset.Close
objConnection.Close

Set objArgs = nothing
Set objFSO = nothing
Set objDictionary = nothing
Set objTextFile = nothing
Set objLogFile = nothing
Set objWMIService = nothing