Const ForReading = 1
Const ForAppending = 8
Const adOpenStatic = 3
Const adLockOptimistic = 3
Const adUseClient = 3

Set objConnection = CreateObject("ADODB.Connection")
Set objRecordset = CreateObject("ADODB.Recordset")

Set objArgs = WScript.Arguments
Set objFSO = CreateObject("Scripting.FileSystemObject")
Set objDictionary = CreateObject("Scripting.Dictionary")
Set objTextFile = objFSO.OpenTextFile(objArgs(0), ForReading)
i = 0
 
Do While objTextFile.AtEndOfStream <> True
  strNextLine = objTextFile.Readline
  objDictionary.Add i, strNextLine
  i = i + 1
Loop

objConnection.Open "DSN=Computer_Management;"

For Each objItem in objDictionary
  ON ERROR RESUME NEXT
  strcomputer = objDictionary.Item(objItem)
  Set objWMIService = GetObject("winmgmts:" _
    & "{impersonationLevel=impersonate}!\\" & strComputer & "\root\cimv2")
  Set colSettings3 = objWMIService.ExecQuery _
    ("Select * from Win32_Processor")
  objRecordset.CursorLocation = adUseClient
  objRecordset.Open "SELECT * FROM Win32_Processor " , objConnection, _
    adOpenStatic, adLockOptimistic
  For Each objProcessor in colSettings3
    objRecordset.AddNew
    objRecordset("strcomputer") = strcomputer
    objRecordset("Processor_Architecture") = objProcessor.Architecture
    objRecordset("Processor_Description") = objProcessor.Description
    objRecordset.Update
  Next
Next

objRecordset.Close
objConnection.Close

Set objArgs = nothing
Set objFSO = nothing
Set objDictionary = nothing
Set objTextFile = nothing
Set objLogFile = nothing
Set objWMIService = nothing