Const ForReading = 1
Const ForAppending = 8
Const adOpenStatic = 3
Const adLockOptimistic = 3
Const adUseClient = 3

Set objConnection = CreateObject("ADODB.Connection")
Set objRecordset = CreateObject("ADODB.Recordset")

Set objArgs = WScript.Arguments
Set objFSO = CreateObject("Scripting.FileSystemObject")
Set objDictionary = CreateObject("Scripting.Dictionary")
Set objTextFile = objFSO.OpenTextFile(objArgs(0), ForReading)
i = 0
 
Do While objTextFile.AtEndOfStream <> True
  strNextLine = objTextFile.Readline
  objDictionary.Add i, strNextLine
  i = i + 1
Loop

objConnection.Open "DSN=Computer_Management;"

For Each objItem in objDictionary
  On Error Resume Next
  strcomputer = objDictionary.Item(objItem)
  Set objWMIService = GetObject("winmgmts:" _
    & "{impersonationLevel=impersonate}!\\" & strComputer & "\root\cimv2")
  Set colItems = objWMIService.ExecQuery("Select * from Win32_PhysicalMemory",,48)
  objRecordset.CursorLocation = adUseClient
  objRecordset.Open "SELECT * FROM Win32_PhysicalMemory " , objConnection, _
    adOpenStatic, adLockOptimistic 
  For Each objItem2 in colItems
    objRecordset.AddNew
    objRecordset("strcomputer") = strcomputer
    objRecordset("objItem_BankLabel") = objItem2.BankLabel
    objRecordset("objItem_Capacity") = objItem2.Capacity
    objRecordset("objItem_DataWidth") = objItem2.DataWidth
    objRecordset("objItem_Description") = objItem2.Description
    objRecordset("objItem_DeviceLocator") = objItem2.DeviceLocator
    objRecordset("objItem_FormFactor") = objItem2.FormFactor
    objRecordset("objItem_HotSwappable") = objItem2.HotSwappable
    objRecordset("objItem_Manufacturer") = objItem2.Manufacturer
    objRecordset("objItem_MemoryType") = objItem2.MemoryType
    objRecordset("objItem_Name") = objItem2.Name
    objRecordset("objItem_PartNumber") = objItem2.PartNumber
    objRecordset("objItem_PositionInRow") = objItem2.PositionInRow
    objRecordset("objItem_Speed") = objItem2.Speed
    objRecordset("objItem_Tag") = objItem2.Tag
    objRecordset("objItem_TypeDetail") = objItem2.TypeDetail
    objRecordset.Update
  Next
Next

objRecordset.Close
objConnection.Close

Set objArgs = nothing
Set objFSO = nothing
Set objDictionary = nothing
Set objTextFile = nothing
Set objLogFile = nothing
Set objWMIService = nothing
