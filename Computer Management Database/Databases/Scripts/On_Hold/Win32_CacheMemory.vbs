Const ForReading = 1
Const ForAppending = 8
Const adOpenStatic = 3
Const adLockOptimistic = 3
Const adUseClient = 3

Set objConnection = CreateObject("ADODB.Connection")
Set objRecordset = CreateObject("ADODB.Recordset")

Set objArgs = WScript.Arguments
Set objFSO = CreateObject("Scripting.FileSystemObject")
Set objDictionary = CreateObject("Scripting.Dictionary")
Set objTextFile = objFSO.OpenTextFile(objArgs(0), ForReading)
i = 0
 
Do While objTextFile.AtEndOfStream <> True
  strNextLine = objTextFile.Readline
  objDictionary.Add i, strNextLine
  i = i + 1
Loop

objConnection.Open "DSN=Computer_Management;"

For Each objItem in objDictionary
On Error Resume Next
strcomputer = objDictionary.Item(objItem)
Set objWMIService = GetObject("winmgmts:\\" & strComputer & "\root\cimv2")
Set colItems = objWMIService.ExecQuery("Select * from Win32_CacheMemory")
objRecordset.CursorLocation = adUseClient
  objRecordset.Open "SELECT * FROM Win32_CacheMemory " , objConnection, _
    adOpenStatic, adLockOptimistic 
For Each objItem2 in colItems
    objRecordset.AddNew
    objRecordset("objItem.Access") = objItem2.Access
    For Each objElement1 In objItem2.AdditionalErrorData
        objRecordset("strcomputer") = strcomputer
        objRecordset("objItem_AdditionalErrorData") = objElement1
    Next
    objRecordset("objItem_Associativity") = objItem2.Associativity
    objRecordset("objItem_Availability") = objItem2.Availability
    objRecordset("objItem_BlockSize") = objItem2.BlockSize
    objRecordset("objItem_CacheSpeed") = objItem2.CacheSpeed
    objRecordset("objItem_CacheType") = objItem2.CacheType
    For Each objElement2 In objItem2.CurrentSRAM
        objRecordset("strcomputer") = strcomputer
        objRecordset("objItem_CurrentSRAM") = objElement2
    Next
    objRecordset("strcomputer") = strcomputer 
    objRecordset("objItem_Description") = objItem2.Description
    objRecordset("objItem_DeviceID") = objItem2.DeviceID
    objRecordset("objItem_ErrorCorrectType") = objItem2.ErrorCorrectType
    objRecordset("objItem_InstalledSize") = objItem2.InstalledSize
    objRecordset("objItem_Level") = objItem2.Level
    objRecordset("objItem_Location") = objItem2.Location
    objRecordset("objItem_MaxCacheSize") = objItem2.MaxCacheSize
    objRecordset("objItem_Name") = objItem2.Name
    objRecordset("objItem_NumberOfBlocks") = objItem2.NumberOfBlocks
    objRecordset("objItem_StatusInfo") = objItem2.StatusInfo
    objRecordset("objItem_WritePolicy") = objItem2.WritePolicy
    For Each objElement3 In objItem2.SupportedSRAM
      objRecordset("strcomputer") = strcomputer
      objRecordset("objItem_SupportedSRAM") = objElement3
      objRecordset.Update
    Next
Next
Next

objRecordset.Close
objConnection.Close

Set objArgs = nothing
Set objFSO = nothing
Set objDictionary = nothing
Set objTextFile = nothing
Set objLogFile = nothing
Set objWMIService = nothing