Const ForReading = 1
Const ForAppending = 8
Const adOpenStatic = 3
Const adLockOptimistic = 3
Const adUseClient = 3

Set objConnection = CreateObject("ADODB.Connection")
Set objRecordset = CreateObject("ADODB.Recordset")

Set objArgs = WScript.Arguments
Set objFSO = CreateObject("Scripting.FileSystemObject")
Set objDictionary = CreateObject("Scripting.Dictionary")
Set objTextFile = objFSO.OpenTextFile(objArgs(0), ForReading)
i = 0
 
Do While objTextFile.AtEndOfStream <> True
  strNextLine = objTextFile.Readline
  objDictionary.Add i, strNextLine
  i = i + 1
Loop

objConnection.Open "DSN=Computer_Management;"

For Each objItem in objDictionary
On Error Resume Next
strcomputer = objDictionary.Item(objItem)
Set objWMIService = GetObject("winmgmts:\\" & strComputer & "\root\cimv2")
Set colItems2 = objWMIService.ExecQuery("Select * from Win32_SystemSlot",,48)
objRecordset.CursorLocation = adUseClient
  objRecordset.Open "SELECT * FROM Win32_SystemSlot " , objConnection, _
    adOpenStatic, adLockOptimistic 
For Each objItem2 in colItems2
    objRecordset.AddNew
    objRecordset("strcomputer") = strcomputer
    For Each strConnectorPinout in objItem2.ConnectorPinout
        objRecordset("objItem_ConnectorPinout") = strConnectorPinout
    Next
    For Each strVccVoltageSupport in objItem2.VccMixedVoltageSupport
        objRecordset("objItem_VccMixedVoltageSupport") = strVccVoltageSupport
    Next
    For Each strVppVoltageSupport in objItem2.VppMixedVoltageSupport
        objRecordset("objItem_VppMixedVoltageSupport") = strVppVoltageSupport
    Next
    objRecordset("strComputer") = strcomputer
    objRecordset("objItem_ConnectorType") = objItem2.ConnectorType
    objRecordset("objItem_CurrentUsage") = objItem2.CurrentUsage
    objRecordset("objItem_Description") = objItem2.Description
    objRecordset("objItem_HeightAllowed") = objItem2.HeightAllowed
    objRecordset("objItem_LengthAllowed") = objItem2.LengthAllowed
    objRecordset("objItem_Manufacturer") = objItem2.Manufacturer
    objRecordset("objItem_MaxDataWidth") = objItem2.MaxDataWidth
    objRecordset("objItem_Model") = objItem2.Model
    objRecordset("objItem_Name") = objItem2.Name
    objRecordset("objItem_Number") = objItem2.Number
    objRecordset("objItem_PMESignal") = objItem2.PMESignal
    objRecordset("objItem_Shared") = objItem2.Shared
    objRecordset("objItem_SlotDesignation") = objItem2.SlotDesignation
    objRecordset("objItem_SupportsHotPlug") = objItem2.SupportsHotPlug
    objRecordset("objItem_Tag") = objItem2.Tag
    objRecordset("objItem_ThermalRating") = objItem2.ThermalRating
    objRecordset("objItem_Version") = objItem2.Version
    objRecordset.Update
  Next
Next

objRecordset.Close
objConnection.Close

Set objArgs = nothing
Set objFSO = nothing
Set objDictionary = nothing
Set objTextFile = nothing
Set objLogFile = nothing
Set objWMIService = nothing