Const ForReading = 1
Const ForAppending = 8
Const adOpenStatic = 3
Const adLockOptimistic = 3
Const adUseClient = 3

Set objConnection = CreateObject("ADODB.Connection")
Set objRecordset = CreateObject("ADODB.Recordset")

Set objArgs = WScript.Arguments
Set objFSO = CreateObject("Scripting.FileSystemObject")
Set objDictionary = CreateObject("Scripting.Dictionary")
Set objTextFile = objFSO.OpenTextFile(objArgs(0), ForReading)
i = 0
 
Do While objTextFile.AtEndOfStream <> True
  strNextLine = objTextFile.Readline
  objDictionary.Add i, strNextLine
  i = i + 1
Loop

objConnection.Open "DSN=Computer_Management;"

For Each objItem in objDictionary
On Error Resume Next
strcomputer = objDictionary.Item(objItem)
Set objWMIService = GetObject("winmgmts:\\" & strComputer & "\root\cimv2")
Set colItems = objWMIService.ExecQuery("Select * from Win32_DMAChannel")
objRecordset.CursorLocation = adUseClient
  objRecordset.Open "SELECT * from Win32_DMAChannel " , objConnection, _
    adOpenStatic, adLockOptimistic 
For Each objItem2 in colItems
    objRecordset.AddNew
    objRecordset("strcomputer") = strcomputer
    objRecordset("objItem_AddressSize") = objItem2.AddressSize
    objRecordset("objItem_Availability") = objItem2.Availability
    objRecordset("objItem_ByteMode") = objItem2.ByteMode
    objRecordset("objItem_ChannelTiming") = objItem2.ChannelTiming
    objRecordset("objItem_DMAChannel") = objItem2.DMAChannel
    objRecordset("objItem_MaxTransferSize") = objItem2.MaxTransferSize
    objRecordset("objItem_Name") = objItem2.Name
    objRecordset("objItem_TypeCTiming") = objItem2.TypeCTiming
    objRecordset("objItem_WordMode") = objItem2.WordMode
    objRecordset.Update
Next
Next

objRecordset.Close
objConnection.Close

Set objArgs = nothing
Set objFSO = nothing
Set objDictionary = nothing
Set objTextFile = nothing
Set objLogFile = nothing
Set objWMIService = nothing