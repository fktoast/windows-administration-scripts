Const ForReading = 1
Const ForAppending = 8
Const adOpenStatic = 3
Const adLockOptimistic = 3
Const adUseClient = 3

Set objConnection = CreateObject("ADODB.Connection")
Set objRecordset = CreateObject("ADODB.Recordset")

Set objArgs = WScript.Arguments
Set objFSO = CreateObject("Scripting.FileSystemObject")
Set objDictionary = CreateObject("Scripting.Dictionary")
Set objTextFile = objFSO.OpenTextFile(objArgs(0), ForReading)
i = 0
 
Do While objTextFile.AtEndOfStream <> True
  strNextLine = objTextFile.Readline
  objDictionary.Add i, strNextLine
  i = i + 1
Loop

objConnection.Open "DSN=Computer_Management;"

For Each objItem in objDictionary
On Error Resume Next
strcomputer = objDictionary.Item(objItem)
Set objWMIService = GetObject("winmgmts:" _
    & "{impersonationLevel=impersonate}!\\" & strComputer & "\root\cimv2")
Set colItems = objWMIService.ExecQuery("Select * from Win32_BaseBoard")
objRecordset.CursorLocation = adUseClient
  objRecordset.Open "SELECT * FROM Win32_Baseboard " , objConnection, _
    adOpenStatic, adLockOptimistic 
  For Each objItem2 in colItems
    objRecordset("strcomputer") = strcomputer
    objRecordset("objItem_Depth") = objItem2.Depth
    objRecordset("objItem_Description") = objItem2.Description    
    objRecordset("objItem_Height") = objItem2.Height
    objRecordset("objItem_HostingBoard ") = objItem2.HostingBoard
    objRecordset("objItem_HotSwappable") = objItem2.HotSwappable
    objRecordset("objItem_Manufacturer") = objItem2.Manufacturer
    objRecordset("objItem_Model") = objItem2.Model
    objRecordset("objItem_Name") = objItem2.Name
    objRecordset("objItem_OtherIdentifyingInfo") = objItem2.OtherIdentifyingInfo
    objRecordset("objItem_PartNumber") = objItem2.PartNumber
    objRecordset("objItem_PoweredOn") = objItem2.PoweredOn
    objRecordset("objItem_Product") = objItem2.Product
    objRecordset("objItem_Removable") = objItem2.Removable
    objRecordset("objItem_Replaceable") = objItem2.Replaceable
    objRecordset("objItem_RequirementsDescription") = objItem2.RequirementsDescription
    objRecordset("objItem_RequiresDaughterBoard") = objItem2.RequiresDaughterBoard
    objRecordset("objItem_SerialNumber") = objItem2.SerialNumber
    objRecordset("objItem_SKU") = objItem2.SKU
    objRecordset("objItem_SlotLayout") = objItem2.SlotLayout
    objRecordset("objItem_SpecialRequirements") = objItem2.SpecialRequirements
    objRecordset("objItem_Tag") = objItem2.Tag
    objRecordset("objItem_Version") = objItem2.Version
    objRecordset("objItem_Weight") = objItem2.Weight
    objRecordset("objItem_Width") = objItem2.Width
    objRecordset.Update
  Next
Next

objRecordset.Close
objConnection.Close

Set objArgs = nothing
Set objFSO = nothing
Set objDictionary = nothing
Set objTextFile = nothing
Set objLogFile = nothing
Set objWMIService = nothing