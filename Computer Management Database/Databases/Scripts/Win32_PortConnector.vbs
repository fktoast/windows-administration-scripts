Const ForReading = 1
Const ForAppending = 8
Const adOpenStatic = 3
Const adLockOptimistic = 3
Const adUseClient = 3

Set objConnection = CreateObject("ADODB.Connection")
Set objRecordset = CreateObject("ADODB.Recordset")

Set objArgs = WScript.Arguments
Set objFSO = CreateObject("Scripting.FileSystemObject")
Set objDictionary = CreateObject("Scripting.Dictionary")
Set objTextFile = objFSO.OpenTextFile(objArgs(0), ForReading)
i = 0
 
Do While objTextFile.AtEndOfStream <> True
  strNextLine = objTextFile.Readline
  objDictionary.Add i, strNextLine
  i = i + 1
Loop

objConnection.Open "DSN=Computer_Management;"

For Each objItem in objDictionary
  On Error Resume Next
  strcomputer = objDictionary.Item(objItem)
  Set objWMIService = GetObject("winmgmts:\\" & strComputer & "\root\cimv2")
  Set colItems = objWMIService.ExecQuery("Select * from Win32_PortConnector")
  objRecordset.CursorLocation = adUseClient
  objRecordset.Open "SELECT * FROM Win32_PortConnector " , objConnection, _
    adOpenStatic, adLockOptimistic 
  For Each objItem2 in colItems
    objRecordset.AddNew
    objRecordset("strcomputer") = strcomputer
    objRecordset("objItem_ConnectorPinout") = objItem2.ConnectorPinout
    For Each strConnectorType in objItem2.ConnectorType
        objRecordset("objItem_ConnectorType") = strConnectorType
    Next
    objRecordset("objItem_Description") = objItem2.Description
    objRecordset("objItem_ExternalReferenceDesignator") = objItem2.ExternalReferenceDesignator
    objRecordset("objItem_InternalReferenceDesignator") = objItem2.InternalReferenceDesignator
    objRecordset("objItem_Name") = objItem2.Name
    objRecordset("objItem_PortType") = objItem2.PortType
    objRecordset("objItem_SerialNumber") = objItem2.SerialNumber
    objRecordset("objItem_Tag") = objItem2.Tag
    objRecordset("objItem_Version") = objItem2.Version
    objRecordset.Update
  Next
Next

objRecordset.Close
objConnection.Close

Set objArgs = nothing
Set objFSO = nothing
Set objDictionary = nothing
Set objTextFile = nothing
Set objLogFile = nothing
Set objWMIService = nothing