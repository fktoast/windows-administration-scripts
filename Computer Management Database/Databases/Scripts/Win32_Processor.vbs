Const ForReading = 1
Const ForAppending = 8
Const adOpenStatic = 3
Const adLockOptimistic = 3
Const adUseClient = 3

Set objConnection = CreateObject("ADODB.Connection")
Set objRecordset = CreateObject("ADODB.Recordset")

Set objArgs = WScript.Arguments
Set objFSO = CreateObject("Scripting.FileSystemObject")
Set objDictionary = CreateObject("Scripting.Dictionary")
Set objTextFile = objFSO.OpenTextFile(objArgs(0), ForReading)
i = 0
 
Do While objTextFile.AtEndOfStream <> True
  strNextLine = objTextFile.Readline
  objDictionary.Add i, strNextLine
  i = i + 1
Loop

objConnection.Open "DSN=Computer_Management;"

For Each objItem in objDictionary
  On Error Resume Next
  strcomputer = objDictionary.Item(objItem)
  Set objWMIService = GetObject("winmgmts:\\" & strComputer & "\root\cimv2")
  Set colItems = objWMIService.ExecQuery("Select * from Win32_Processor")
  Set objProcessor = objWMIService.Get("win32_Processor='CPU0'")
  objRecordset.CursorLocation = adUseClient
  objRecordset.Open "SELECT * FROM Win32_Processor " , objConnection, _
    adOpenStatic, adLockOptimistic 
  For Each objItem in colItems
    objRecordset("objItem.AddressWidth") = objItem.AddressWidth
    objRecordset("objItem.Availability") = objItem.Availability
    objRecordset("objItem.CpuStatus") = objItem.CpuStatus
    objRecordset("objItem.CurrentClockSpeed") = objItem.CurrentClockSpeed
    objRecordset("objItem.DataWidth") = objItem.DataWidth
    objRecordset("objItem.Description") = objItem.Description
    objRecordset("objItem.DeviceID") = objItem.DeviceID
    objRecordset("objItem.ExtClock") = objItem.ExtClock
    objRecordset("objItem.Family") = objItem.Family
    objRecordset("objItem.L2CacheSize") = objItem.L2CacheSize
    objRecordset("objItem.L2CacheSpeed") = objItem.L2CacheSpeed
    objRecordset("objItem.Level") = objItem.Level
    objRecordset("objItem.LoadPercentage") = objItem.LoadPercentage
    objRecordset("objItem.Manufacturer") = objItem.Manufacturer
    objRecordset("objItem.MaxClockSpeed") = objItem.MaxClockSpeed
    objRecordset("objItem.Name") = objItem.Name
    objRecordset("objItem.PNPDeviceID") = objItem.PNPDeviceID
    objRecordset("objItem.ProcessorId") = objItem.ProcessorId
    objRecordset("objItem.ProcessorType") = objItem.ProcessorType
    objRecordset("objItem.Revision") = objItem.Revision
    objRecordset("objItem.Role") = objItem.Role
    objRecordset("objItem.SocketDesignation") = objItem.SocketDesignation 
    objRecordset("objItem.StatusInfo") = objItem.StatusInfo
    objRecordset("objItem.Stepping") = objItem.Stepping
    objRecordset("objItem.UniqueId") = objItem.UniqueId
    objRecordset("objItem.UpgradeMethod") = objItem.UpgradeMethod
    objRecordset("objItem.Version") = objItem.Version
    objRecordset("objItem.VoltageCaps") = objItem.VoltageCaps
    If objProcessor.Architecture = 0 Then
      objRecordset("objItem.Architecture") =  "This is an x86 computer."
    ElseIf objProcessor.Architecture = 1 Then
      objRecordset("objItem.Architecture") =  "This is a MIPS computer."
    ElseIf objProcessor.Architecture = 2 Then
      objRecordset("objItem.Architecture") =  "This is an Alpha computer."
    ElseIf objProcessor.Architecture = 3 Then
      objRecordset("objItem.Architecture") =  "This is a PowerPC computer."
    ElseIf objProcessor.Architecture = 6 Then
      objRecordset("objItem.Architecture") =  "This is an ia64 computer."
    Else
      objRecordset("objItem.Architecture") = "The computer type could not be determined."
    End If
  Next
Next

objRecordset.Close
objConnection.Close

Set objArgs = nothing
Set objFSO = nothing
Set objDictionary = nothing
Set objTextFile = nothing
Set objLogFile = nothing
Set objWMIService = nothing