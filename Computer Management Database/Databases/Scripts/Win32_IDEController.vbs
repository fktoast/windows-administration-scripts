Const ForReading = 1
Const ForAppending = 8
Const adOpenStatic = 3
Const adLockOptimistic = 3
Const adUseClient = 3

Set objConnection = CreateObject("ADODB.Connection")
Set objRecordset = CreateObject("ADODB.Recordset")

Set objArgs = WScript.Arguments
Set objFSO = CreateObject("Scripting.FileSystemObject")
Set objDictionary = CreateObject("Scripting.Dictionary")
Set objTextFile = objFSO.OpenTextFile(objArgs(0), ForReading)
i = 0
 
Do While objTextFile.AtEndOfStream <> True
  strNextLine = objTextFile.Readline
  objDictionary.Add i, strNextLine
  i = i + 1
Loop

objConnection.Open "DSN=Computer_Management;"

For Each objItem in objDictionary
  On Error Resume Next
  strcomputer = objDictionary.Item(objItem)
  Set objWMIService = GetObject("winmgmts:" _
    & "{impersonationLevel=impersonate}!\\" & strComputer & "\root\cimv2")
  Set colItems = objWMIService.ExecQuery("Select * from Win32_IDEController")
  objRecordset.CursorLocation = adUseClient
  objRecordset.Open "SELECT * FROM Win32_IDEController " , objConnection, _
    adOpenStatic, adLockOptimistic 
  For Each objItem1 in colItems
    objRecordset.AddNew
    objRecordset("strcomputer") = strcomputer
    objRecordset("objItem_ConfigManagerErrorCode") = objItem.ConfigManagerErrorCode
    objRecordset("objItem_ConfigManagerUserConfig") = objItem.ConfigManagerUserConfig
    objRecordset("objItem_DeviceID") = objItem.DeviceID
    objRecordset("objItem_Manufacturer") = objItem.Manufacturer
    objRecordset("objItem_Name") = objItem.Name
    objRecordset("objItem_PNPDeviceID") = objItem.PNPDeviceID
    objRecordset("objItem_ProtocolSupported") = objItem.ProtocolSupported
    objRecordset.Update
  Next
Next
objRecordset.Close
objConnection.Close

Set objArgs = nothing
Set objFSO = nothing
Set objDictionary = nothing
Set objTextFile = nothing
Set objLogFile = nothing
Set objWMIService = nothing

