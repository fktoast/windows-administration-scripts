Const ForAppending = 8
Set objArgs = WScript.Arguments
Set objFSO = CreateObject("Scripting.FileSystemObject")
Const ForReading = 1
Set objDictionary = CreateObject("Scripting.Dictionary")
Set objTextFile = objFSO.OpenTextFile(objArgs(0), ForReading)
i = 0
 
Do While objTextFile.AtEndOfStream <> True
  strNextLine = objTextFile.Readline
  objDictionary.Add i, strNextLine
  i = i + 1
Loop

On Error Resume Next

For Each objItem in objDictionary
  strcomputer = objDictionary.Item(objItem)
  Set objWMIService = GetObject("winmgmts:" _
      & "{impersonationLevel=impersonate}!\\" & strComputer & "\root\cimv2")
  Set objLogFile = objFSO.OpenTextFile("C:\FKOCH\Administrative Archive\Microsoft\" _
      & "Scripts\VBScript\WMI\Complete\Software\Logs\AppServ_TLF.csv", _ 
      ForAppending, True)
  objLogFile.Write _
      ("Computer,Applications") 
  objLogFile.Writeline
  Set colApps = objWMIService.ExecQuery("Select * from Win32_ApplicationService")
  For Each objApp in colApps
      objLogFile.Write(strcomputer) & ","
      objLogFile.Write(objApp.Name) & ","
      objLogFile.writeline
  Next
objLogFile.Close
Next 
For Each objItem in objDictionary
  strcomputer = objDictionary.Item(objItem)
  Set objWMIService = GetObject("winmgmts:" _
      & "{impersonationLevel=impersonate}!\\" & strComputer & "\root\cimv2")
  Set objLogFile = objFSO.OpenTextFile("C:\FKOCH\Administrative Archive\Microsoft\" _
      & "Scripts\VBScript\WMI\Complete\Software\Logs\software_TLF.csv", _ 
      ForAppending, True)
  objLogFile.Write _
      ("Computer,Caption,Description,Identifying Number,Install Date," _
      & "Install Location,Install State,Name,Package Cache,SKU Number,Vendor,Version") 
  objLogFile.Writeline
  Set colSoftware = objWMIService.ExecQuery _
      ("Select * from Win32_Product")
  For Each objSoftware in colSoftware
      objLogFile.Write(strcomputer) & ","
      objLogFile.Write(objSoftware.Caption) & ","
      objLogFile.Write(objSoftware.Description) & ","
      objLogFile.Write(objSoftware.IdentifyingNumber) & ","
      objLogFile.Write(objSoftware.InstallDate) & ","
      objLogFile.Write(objSoftware.InstallLocation) & ","
      objLogFile.Write(objSoftware.InstallState) & ","
      objLogFile.Write(objSoftware.Name) & ","
      objLogFile.Write(objSoftware.PackageCache) & ","
      objLogFile.Write(objSoftware.SKUNumber) & ","
      objLogFile.Write(objSoftware.Vendor) & ","
      objLogFile.Write(objSoftware.Version) & ","
      objLogFile.writeline
  Next
objLogFile.Close
Next
For Each objItem in objDictionary
  strcomputer = objDictionary.Item(objItem)
  Set objWMIService = GetObject("winmgmts:" _
      & "{impersonationLevel=impersonate}!\\" & strComputer & "\root\cimv2")
  Set objLogFile = objFSO.OpenTextFile("C:\FKOCH\Administrative Archive\Microsoft\" _
      & "Scripts\VBScript\WMI\Complete\Software\Logs\AppDCOM_TLF.csv", _ 
      ForAppending, True)
  objLogFile.Write _
      ("Computer,Application ID,Name") 
  objLogFile.Writeline
  Set colDCOM = objWMIService.ExecQuery("Select * from Win32_DCOMApplication")
  For Each objDCOM in colDCOM
      objLogFile.Write(strcomputer) & ","
      objLogFile.Write(objDCOM.AppID) & ","
      objLogFile.Write(objDCOM.Name) & ","
      objLogFile.writeline
  Next
objLogFile.Close
Next
Set objArgs = nothing
Set objFSO = nothing
Set objDictionary = nothing
Set objTextFile = nothing
Set objLogFile = nothing