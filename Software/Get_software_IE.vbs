Set objArgs = WScript.Arguments
Set objFSO = CreateObject("Scripting.FileSystemObject")
Const ForReading = 1
Set objDictionary = CreateObject("Scripting.Dictionary")
Set objTextFile = objFSO.OpenTextFile(objArgs(0), ForReading)
i = 0
 
Do While objTextFile.AtEndOfStream <> True
  strNextLine = objTextFile.Readline
  objDictionary.Add i, strNextLine
  i = i + 1
Loop

Set objExplorer = WScript.CreateObject("InternetExplorer.Application")
objExplorer.Navigate "about:blank"   
objExplorer.ToolBar = 0
objExplorer.StatusBar = 0
objExplorer.Width=400
objExplorer.Height = 150 
objExplorer.Left = 0
objExplorer.Top = 0
Do While (objExplorer.Busy)
    Wscript.Sleep 50
Loop    
objExplorer.Visible = 1             
objExplorer.Document.Body.InnerHTML = "Retrieving service information. " _
    & "This might take several minutes to complete."

On Error Resume Next

For Each objItem in objDictionary
  strcomputer = objDictionary.Item(objItem)
  Set objWMIService = GetObject("winmgmts:" _
      & "{impersonationLevel=impersonate}!\\" & strComputer & "\root\cimv2")
  Set colApps = objWMIService.ExecQuery("Select * from Win32_ApplicationService")
  For Each objApp in colApps
      strnme = "AppName: " & objApp.Name
      stroutput = stroutput & "<br>" & "<--------- " & strcomputer & " --------->" & "<br>" & strnme & "<br>"
  Next
Next 
For Each objItem in objDictionary
  strcomputer = objDictionary.Item(objItem)
  Set objWMIService = GetObject("winmgmts:" _
      & "{impersonationLevel=impersonate}!\\" & strComputer & "\root\cimv2")
  Set colSoftware = objWMIService.ExecQuery _
      ("Select * from Win32_Product")
  For Each objSoftware in colSoftware
      strCapt = objSoftware.Caption & vbtab 
      strDesc = objSoftware.Description & vbtab 
      strIdNu = objSoftware.IdentifyingNumber & vbtab 
      strInDa = objSoftware.InstallDate & vbtab 
      strInLo = objSoftware.InstallLocation & vbtab 
      strInSt = objSoftware.InstallState & vbtab 
      strName = objSoftware.Name & vbtab 
      strPkCa = objSoftware.PackageCache & vbtab 
      strSkuN = objSoftware.SKUNumber & vbtab 
      strVndr = objSoftware.Vendor & vbtab 
      strVers = objSoftware.Version & vbtab
      stroutput = stroutput & "<br>" & "<--------- " & strcomputer & " --------->" & "<br>" & _
      "Caption: ------------- " & vbtab & strCapt & "<br>" & _
      "Description: --------- " & vbtab & strDesc & "<br>" & _
      "Identifying Number: - " & vbtab & strIdNu & "<br>" & _
      "Install Date: --------- " & vbtab & strInDa & "<br>" & _
      "Install Location: ----- " & vbtab & strInLo & "<br>" & _
      "Install State: --------- " & vbtab & strInSt & "<br>" & _
      "Name: -------------- " & vbtab & strName & "<br>" & _ 
      "Package Cache: ---- " & vbtab & strPkCa & "<br>" & _
      "SKU Number: ------ " & vbtab & strSkuN & "<br>" & _
      "Vendor: ------------ " & vbtab & strVndr & "<br>" & _
      "Version: ------------ " & vbtab & strVers & "<br>"
  Next
Next
For Each objItem in objDictionary
  strcomputer = objDictionary.Item(objItem)
  Set objWMIService = GetObject("winmgmts:" _
      & "{impersonationLevel=impersonate}!\\" & strComputer & "\root\cimv2")
  Set colDCOM = objWMIService.ExecQuery("Select * from Win32_DCOMApplication")
  For Each objDCOM in colDCOM
      strApplId = "Application ID: " & vbtab & objDCOM.AppID
      strAppNm = "Name: " & vbtab & objDCOM.Name
      stroutput = stroutput & "<br>" & "<--------- " & strcomputer & " --------->" & "<br>" & _
      strAppNm & "<br>" & strApplId & "<br>"
  Next
Next
objExplorer.Width=600
objExplorer.Height = 500
objExplorer.Document.Body.InnerHTML = stroutput
Set objArgs = nothing
Set objFSO = nothing
Set objDictionary = nothing
Set objTextFile = nothing
Set objExplorer = nothing