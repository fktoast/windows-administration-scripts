strDcinfo = "OU=scripttest,dc=avalonconsult,dc=com"
strLDAP = "LDAP://"

Set objExplorer = WScript.CreateObject("InternetExplorer.Application")
objExplorer.Navigate "about:blank"   
objExplorer.ToolBar = 0
objExplorer.StatusBar = 0
objExplorer.Width=400
objExplorer.Height = 400 
objExplorer.Left = 0
objExplorer.Top = 0

Do While (objExplorer.Busy)
    Wscript.Sleep 600
Loop    

objExplorer.Visible = 1             
objExplorer.Document.Body.InnerHTML = "Retrieving service information. " _
    & "This might take several minutes to complete."

Set objArgs = WScript.Arguments
Const ForReading = 1
Set objDictionary = CreateObject("Scripting.Dictionary")
Set objFSO = CreateObject("Scripting.FileSystemObject")
Set objTextFile = objFSO.OpenTextFile(objArgs(0), ForReading)



Do Until objTextFile.AtEndOfStream
    strNextLine = objTextFile.Readline
    arrUserList = Split(strNextLine , ",")
    strUsername = arrUserList(0)
    strPassword = arrUserList(1)
    strFirstName = arrUserList(2)
    strLastName = arrUserList(3)    
    strEmail = arrUserList(4)
    strCN = arrUserlist(5)
    Set objOU = GetObject(strLDAP & strDcinfo)
    Set objUser = objOU.Create("User", "cn= " & strUsername)
    objUser.Put "displayname", strCN
    objUser.Put "name", strCN
    objUser.Put "sAMAccountName", strUsername
    objUser.Put "mail", strEmail
    objUser.Put "givenname", strFirstName
    objUser.Put "sn", strlastname
    objUser.SetInfo
    Set objUser2 = GetObject(strLDAP & "cn=" & strUsername & "," & strDcinfo)
    objUser2.SetPassword strPassword
    objUser2.AccountDisabled = FALSE
    If objUser2.AccountDisabled = FALSE Then
        strAccountStatus = "This account is enabled."
    Else
        strAccountStatus = "This account is disabled."
    End If
    objUser2.SetInfo
    strTemp = strTemp & "Account Name ---> " & vbTab & strUsername & vbtab & strFirstName & vbtab & strlastname & "<br>"
    strTemp = strTemp & "Password --------> " & vbTab & strPassword & "<br>"
    strTemp = strTemp & "Account Status:     " & vbTab & strAccountStatus & "<br>" & "<br>"
Loop

objExplorer.Document.Body.InnerHTML = strtemp

Wscript.Sleep 500
set objargs = nothing
Set objOU = nothing
Set objUser = nothing
Set objUser2 = nothing
Set objectexplorer = nothing
Set objDictionary = nothing
Set objFSO = nothing
Set objWMIService = nothing
Set objTextFile = nothing

Wscript.Quit