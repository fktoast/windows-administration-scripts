Const ForAppending = 8
Set objFSO = CreateObject("Scripting.FileSystemObject")
Set objTextFile = objFSO.OpenTextFile _
    ("Users.txt", ForAppending, True)
Set colServices =  GetObject("winmgmts:").ExecQuery _
    ("Select * from Win32_Service")

Set objDictionary = CreateObject("Scripting.Dictionary")
i = 0
Set objOU = GetObject("LDAP://OU=Corporate, DC=corp, DC=leatherfactory, DC=com")
objOU.Filter = Array("User")
For Each objUser in objOU 
    objDictionary.Add i, objUser.CN
    i = i + 1
Next
For Each objItem in objDictionary
  strUser = objDictionary.Item(objItem)
  Set objUser = GetObject _
    ("LDAP://cn=" & struser & ",ou=Corporate,dc=corp,dc=leatherfactory,dc=com")
  objUser.Put "scriptPath", "startup.bat"
  objUser.SetInfo
  objTextFile.WriteLine(strUser & vbTab)
Next
objTextFile.Close
