Const ForAppending = 8
Const ADS_PROPERTY_CLEAR = 1
Set objFSO = CreateObject("Scripting.FileSystemObject")
Set objTextFile = objFSO.OpenTextFile _
    ("Users.txt", ForAppending, True)
Set colServices =  GetObject("winmgmts:").ExecQuery _
    ("Select * from Win32_Service")
Set objDictionary = CreateObject("Scripting.Dictionary")
i = 0
Set objOU = GetObject("LDAP://CN=Users, DC=corp, DC=leatherfactory, DC=com")
objOU.Filter = Array("User")
For Each objUser in objOU 
    objDictionary.Add i, objUser.CN
    i = i + 1
Next
For Each objItem in objDictionary
  strUser = objDictionary.Item(objItem)
  Set objUser = GetObject _
    ("LDAP://cn=" & struser & ",cn=Users,dc=corp,dc=leatherfactory,dc=com")
'  strSP = objUser.Get("scriptPath")'
  objUser.PutEX ADS_PROPERTY_CLEAR, "scriptPath", 0
  objUser.SetInfo
  objTextFile.WriteLine(strUser & vbTab & strSP)
Next
objTextFile.Close
Set objFSO = nothing
Set objTextFile = nothing
Set objDictionary = nothing
Set objOU = nothing
