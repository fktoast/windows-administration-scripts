Set objDictionary = CreateObject("Scripting.Dictionary")
Set objuser = GetObject _
   ("LDAP://cn=Users,dc=corp,dc=leatherfactory,dc=com")
DO While objUser.AtEndOfStream <> True
   strNextuser = objuser.Readline
   objdictionary.Add i, strnextuser
   i = i + 1
Loop

For Each objUser in objDictionary
CheckForUser(objUser)
Sub CheckForUser(samAccountName)
    dtStart = TimeValue(Now())
    strUserName = samAccountName
    Set objConnection = CreateObject("ADODB.Connection")
    objConnection.Open "Provider=ADsDSOObject;"
 
    Set objCommand = CreateObject("ADODB.Command")
    objCommand.ActiveConnection = objConnection
 
    objCommand.CommandText = _
        "<LDAP://dc=corp,dc=leatherfactory,dc=com>;(&(objectCategory=User)" & _
            "(samAccountName=" & strUserName & "));samAccountName;subtree"
   
    Set objRecordSet = objCommand.Execute
 
    If objRecordset.RecordCount = 0 Then
        WScript.Echo "sAMAccountName: " & strUserName & " does not exist."
    Else
        WScript.Echo strUserName & " exists."
    End If
 
    objConnection.Close
    WScript.Echo "Script completed in " & _
        Second(TimeValue(now()) - dtStart) & _
            " second or less."
End Sub
