@echo off
title Multiple Choice Menu
:home
cls
echo.
echo Select an Image:
echo =============
echo.
echo 1) Dos Menu
echo 2) HTML Wizard
echo 3) Exit
echo.
set /p web=Type option:
if "%web%"=="1" goto dosmenu
if "%web%"=="2" goto htmlwizard
if "%web%"=="3" exit
goto home

:dosmenu
call w:\scripts\dosmenu.bat
goto home

:htmlwizard
call w:\scripts\networkui.hta %1
goto home