
@echo off
title Multiple Choice Menu
:home
cls
echo.
echo Select an Image:
echo =============
echo.
echo 1) HP EliteBook 8460P x86 (Office 2010)
echo 2) HP EliteBook 8440P x86 (Office 2007)
echo 3) HP EliteBook 8440P HR Image
echo 4) HP EliteBook 8440P x86 (Office 2010)
echo 5) HP EliteBook 8440P x64 (USE 64-BIT WINPE DISK!)
echo 6) HP EliteSFF 8000 x86 (Office 2007)
echo 7) HP EliteSFF 8000 x86 (Office 2010)
echo 8) HP EliteSFF 8000 x64 (USE 64-BIT WINPE DISK!)
echo 9) HP EliteBook 2530P x86 (Office 2007)
echo 10) HP EliteBook 2530P x86 (Office 2010)
echo 11) HP EliteBook 2530P x64 (USE 64-BIT WINPE DISK!)
echo 12) Dell Latitude E6400 x86 (Office 2010)
echo 13) Main Menu
echo 14) Exit
echo.
set /p web=Type option:
if "%web%"=="1" goto hpeb8460px86off10
if "%web%"=="2" goto hpeb8440px86off07
if "%web%"=="3" goto hpeb8440phr
if "%web%"=="4" goto hpeb8440px86off10
if "%web%"=="5" goto hpeb8400px64
if "%web%"=="6" goto hpesff8000x86off07
if "%web%"=="7" goto hpesff8000x86off10
if "%web%"=="8" goto hpesff8000x64
if "%web%"=="9" goto hpeb2530px86off07
if "%web%"=="10" goto hpeb2530px86off10
if "%web%"=="11" goto hpeb2530px68
if "%web%"=="12" goto dllte6400x86off10
if "%web%"=="13" goto mainmenu
if "%web%"=="14" exit
goto home

:mainmenu
call w:\scripts\dos-ui-options.bat

:checkdisk
If exist W:\scripts\diskpart.txt GOTO :image

:image
call W:\scripts\dos-image.bat

:hpeb8460px86off10
set image=8460pW7EntOf10.wim
goto checkdisk


:hpeb8440px86off07
set image=8440pW7Entx86Off07.wim
set bcd=C:\windows\system32\bcdboot C:\windows
goto checkdisk

:hpeb8440phr
set image=hr.wim
set bcd=C:\windows\system32\bcdboot C:\windows
goto checkdisk

:hpeb8440px86off10
set image=8440pW7Entx86Off10.wim
set bcd=C:\windows\system32\bcdboot C:\windows
goto checkdisk

:hpeb8400px64
set image=8440pv3x64.wim
set bcd=bcdboot C:\windows
goto checkdisk

:hpesff8000x86off07
set image=8000W7Entx86Off07.wim
set bcd=C:\windows\system32\bcdboot C:\windows
goto checkdisk

:hpesff8000x86off10
set image=8000W7Entx86Off10.wim
set bcd=C:\windows\system32\bcdboot C:\windows
goto checkdisk

:hpesff8000x64
set image=8000W7Entx64.wim
set bcd=bcdboot C:\windows
goto checkdisk

:hpeb2530px86off07
set image=2530pW7Entx86Off07.wim
set bcd=C:\windows\system32\bcdboot C:\windows
goto checkdisk

:hpeb2530px86off10
set image=2530pW7Entx86Off10.wim
set bcd=C:\windows\system32\bcdboot C:\windows
goto checkdisk

:hpeb2530px64
set image=2530pW7Entx64.wim
set bcd=bcdboot C:\windows
goto checkdisk

:dllte6400x86off10
set image=e6400W7Entx86Off10.wim
set bcd=C:\windows\system32\bcdboot C:\windows
goto checkdisk
